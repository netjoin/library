<?php
namespace library\Model;
//use library\Ext\MongoDB;
use Internet\Database;

class RadioSiteCategory extends Database
{
    public $_name = 'radio_site_category';
    
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $r = $this->fetchRow($arr, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            return $this->add($arr);
        }
        return $r->id;
    }
}
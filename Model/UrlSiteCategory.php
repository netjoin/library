<?php
namespace library\Model;

//use Internet\Database;

class UrlSiteCategory extends \At\Db
{
    public $table_name = 'url_site_category';
    
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $r = $this->fetchRow($arr, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            //return $this->add($arr);
            return $this->from($this->db_name .".$this->table_name")->addSimple($arr);
        }
        return $r->id;
    }
}

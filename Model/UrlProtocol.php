<?php
namespace library\Model;
use Internet\Database;
class UrlProtocol extends Database
{
    public $_name = 'url_protocol';
    public $site_id = 0;
	
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        //print_r($arr);
        $w = array(
            'name' => $arr['name'],
        );
        
        $r = $this->fetchRow($w, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            if (is_array($arr)) {
                $arr = array_merge($where, $arr);//!
            } else {
                $arr = null;
            }
            
            if ($return) {
                return $arr;
            }
            return $this->add($arr);
        }
        return $r->id;
    }
}
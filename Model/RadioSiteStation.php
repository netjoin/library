<?php
namespace library\Model;
use Internet\Database;
class RadioSiteStation extends Database
{
    public $_name = 'radio_site_station';
	
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $w = array(
            'identification' => $arr['identification'],
            'site_id' => $arr['site_id'],
        );
        
        $r = $this->fetchRow($w, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            return $this->add($arr);
        }
        return $r->id;
    }
}
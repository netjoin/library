<?php
namespace library\Model;
class ViewUriScheme extends \At\Db
{
    public $table_name = 'view_uri_scheme';
    public $site_id = 0;
	
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $w = array(
            'detail_id' => $arr['detail_id'],
			'site_id' => $this->site_id,
        );
        
        $r = $this->fetchRow($w, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            return $this->form($this->db_name .'.url_protocol')->addSimple($arr);
        }
        return $r->id;
    }
}
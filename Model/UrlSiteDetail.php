<?php
namespace library\Model;

class UrlSiteDetail extends \At\Db
{
    public $table_name = 'url_site_detail';
    public $site_id = 0;
	
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $w = array(
            'detail_id' => $arr['detail_id'],
			//'site_id' => $this->site_id,
            'site_id' => $arr['site_id'],
			'name' => $arr['name'],
        );
        //
        $r = $this->fetchRow($w, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            return $this->from($this->db_name .".$this->table_name")->addSimple($arr);
        }
		
		foreach ($arr as $key => $value) {
			if (!trim($arr[$key])) {
				unset($arr[$key]);
			}
		}
		
		# print_r(array($arr, $r));exit;
		return $this->update($arr, ['id' => $r->id]);
        return $r->id;
    }
}

<?php
namespace library\Model;

class ViewVideoSiteDetail extends \At\Db
{
    public $table_name = 'view_video_site_detail';
    public $site_id = 0;
	
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $w = array(
            'detail_id' => $arr['detail_id'],
			//'site_id' => $this->site_id,
            'site_id' => $arr['site_id'],
        );
        //
        $r = $this->fetchRow($w, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            return $this->from($this->db_name .".$this->table_name")->addSimple($arr);
        }
		
		foreach ($arr as $key => $value) {
			if (!trim($arr[$key])) {
				unset($arr[$key]);
			}
		}
		
		//print_r(array($arr, $r));
		return $this->update($arr, ['id' => $r->id]);
        return $r->id;
    }
}

<?php
namespace library\Model;

class DomainPrice extends \At\Db
{
    public $table_name = 'domain_price';
    public $site_id = 0;
	
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $w = array(
            'zone' => $arr['zone'],
			'registrar' => $arr['registrar'],
        );
        
        $r = $this->fetchRow($w, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            return $this->from($this->db_name .".$this->table_name")->addSimple($arr);
        }
        return $r->id;
    }
}
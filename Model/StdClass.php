<?php
namespace library\Model;

class StdClass implements \ArrayAccess
{
    public $children = array();
	public $property = array();
	public $container = array();
	
    public function __construct($var = array())
    {
		$this->container = array(
            "one"   => 1,
            "two"   => 2,
            "three" => 3,
        );
		
		foreach ($var as $val) {
			$this->children[] = $val[0];
			$this->property[] = $val[1];
		}
    }
    
    public function children()
    {
        return $this->children;
    }
	
	public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }
    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }
    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }
    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}

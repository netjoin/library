<?php
namespace library\Model;


class DevProject extends \library\Ext\PhpMongoDB
{
    public function __construct($arg = array())
    {
    	echo __LINE__ .' '. __FILE__ . PHP_EOL;
        parent::__construct($arg);
        echo __LINE__ .' '. __FILE__ . PHP_EOL;
    }

    public function test()
    {
    	echo __LINE__ .' '. __FILE__ . PHP_EOL;

    	/*插入数据
		将 name 为"菜鸟教程" 的数据插入到 test 数据库的 runoob 集合中。
		*/
		//$document = ['_id' => new \MongoDB\BSON\ObjectID, 'name' => '菜鸟教程'];
		$document = ['_id' => $this->ObjectID(), 'name' => '菜鸟教程2'];

		$_id= $this->insert($document);

    	

		var_dump($_id);

		$result = $this->executeBulkWrite('test.runoob');

		/*
		读取数据
		这里我们将三个网址数据插入到 test 数据库的 sites 集合，并读取迭代出来：
		*/

		// 插入数据
		
		//$this->driverBulkWrite();
		$this->insert(['x' => 1, 'name'=>'菜鸟教程', 'url' => 'http://www.runoob.com']);
		$this->insert(['x' => 2, 'name'=>'Google', 'url' => 'http://www.google.com']);
		$this->insert(['x' => 3, 'name'=>'taobao', 'url' => 'http://www.taobao.com']);
		$this->executeBulkWrite('test.sites');

/**/
		$filter = ['x' => ['$gt' => 1]];
		$options = [
		    'projection' => ['_id' => 0],
		    'sort' => ['x' => -1],
		];

		// 查询数据
		//$query = new \MongoDB\Driver\Query($filter, $options);
		$query = $this->driverQuery($filter, $options);
		$cursor = $this->executeQuery('test.sites', $query);

		foreach ($cursor as $document) {
		    print_r($document);
		}

/*
		更新数据
		接下来我们将更新 test 数据库 sites 集合中 x 为 2 的数据：
		*/

//$this->driverBulkWrite();
		$this->update(
		    ['x' => 2],
		    ['$set' => ['name' => '菜鸟工具', 'url' => 'tool.runoob.com']],
		    ['multi' => false, 'upsert' => false]
		);

		$result = $this->executeBulkWrite('test.sites');

		/*
		接下来我们使用 "db.sites.find()" 命令查看数据的变化，x 为 2 的数据已经变成了菜鸟工具：

		删除数据
		以下实例删除了 x 为 1 和 x 为 2的数据，注意 limit 参数的区别：
		*/
	
	//$this->driverBulkWrite();
		$this->delete(['x' => 3], ['limit' => 1]);   // limit 为 1 时，删除第一条匹配数据
		//$this->delete(['x' => 2], ['limit' => 0]);   // limit 为 0 时，删除所有匹配数据

		$result = $this->executeBulkWrite('test.sites');

		/*
		更多使用方法请参考：http://php.net/manual/en/book.mongodb.php。
		 */
		echo __LINE__ .' '. __FILE__ . PHP_EOL;
    }
}
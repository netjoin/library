<?php
namespace library\Model;

class RobotLog extends \At\Db
{
    public $table_name = 'robot_log';
    public $site_id = 0;
	
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $w = array(
            'name' => $arr['name'],
			'function' => $arr['function'],
			'method' => $arr['method'],
			'key' => $arr['key'],
			'page' => $arr['page'],
			'type' => $arr['type'],
        );
        
        $r = $this->fetchRow($w, 'id,total');
        if (!$r) {
            $where['status'] = -1;
			$where['total'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            return $this->from($this->db_name .".$this->table_name")->addSimple($arr);
        }
		$arr['status'] = -2;
		$arr['total'] = 1 + $r->total;
		
		return $this->update($arr, ['id' => $r->id]);
        return $r->id;
    }
}
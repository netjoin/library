<?php
namespace library\Model;
class MusicSiteAlbum extends \Internet\Database
{
    public $_name = 'music_site_album';
	
    public function __construct($arg = array())
    {
        parent::__construct($arg);
        $this->time = $_SERVER['REQUEST_TIME'];
    }
    
    public function checkRow($arr, $return = null, $find = array())
    {
        $w = array(
            'identification' => $arr['identification'],
            'site_id' => $arr['site_id'],
        );
        
        $r = $this->fetchRow($w, 'id');
        if (!$r) {
            $where['status'] = 1;
            $where['created'] = $this->time;
            $where['updated'] = $this->time;
            
            $arr = array_merge($where, $arr);//!
            if ($return) {
                return $arr;
            }
            return $this->add($arr);
        }
        return $r->id;
    }
}
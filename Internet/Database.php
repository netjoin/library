<?php 

namespace Internet;

class Database implements DatabaseInterface
{
	public $dbh = null;
	
	public $_name = null;
	public $groupBy = null;
	public $log = array();
	
	public function __construct($arg = array())
	{
           global $_DB, $_BUG;
            $this->bug =& $_BUG;
			
            $this->internet = new Internet;
            if (isset($_GET['debug'])) {
                $this->internet->debug = $_GET['debug'];
            }
            

            if (isset($_DB['dbh'])) {
                $this->dbh = $_DB['dbh'];
                
            } else {
                
		$_CONFIG = require_once "config.php";
		
		$_CONFIG = array(
  'DSN' => 'mysql:host=localhost;dbname=com_urlnk',
  'USERNAME' => 'root',
  'PASSWORD' => 'root',
  'OPTIONS' => array(
    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
  ),
);
		//print_r($_CONFIG);exit;
		
		try {
			$this->dbh = new \PDO($_CONFIG['DSN'], $_CONFIG['USERNAME'], $_CONFIG['PASSWORD'], $_CONFIG['OPTIONS']);
                        if (!$this->dbh) {
                            $this->bug['db'][]= array(array(print_r($this->dbh, true), __FILE__, __LINE__), 'Error');
                        }
                        $_DB['dbh'] = $this->dbh;
			
		} catch (\PDOException $e) {
			$msg = "Connection failed: " . $e->getMessage() . " " . __FILE__;
			$this->bug['db'][]= array(array(__METHOD__ . ' ' . $msg, __FILE__, __LINE__), 'Error');
		}
                
                //print_r($_CONFIG);exit;
                
                
            }
	}
	
	public function addBiaohao($name = null, $sign = '`')
	{
		return $name = $sign . $name . $sign;
	}
	
	public function from()
	{
		$name = $this->_name;
		$NAME = explode('.', $name);
		
		foreach ($NAME as &$nm) {
			$nm = trim($nm);
			
			if (preg_match('/^`(.*)`$/', $nm, $matches)) {
				print_r($matches);
			} else {
				$nm = $this->addBiaohao($nm);
			}
		}
		
		return $from = implode('.', $NAME);
		print_r($NAME);
		exit;
	}
	
	public function _from()
	{
		return $from = ' FROM ' . $this->from();
		
	}
	
	/* 直接 */
	public function update($query)
	{
            return $this->query($query);
	}
	
	public function delete($query)
	{
            $this->log[]= $query;
		return $result = $this->dbh->exec($query);
	}
	
	public function query($query)
	{
            $this->log[]= $query;
		$result = @$this->dbh->query($query, \PDO::FETCH_CLASS, '\stdClass');
		//$result = $this->dbh->query($query);
		if (false === $result) {
			$result = $this->dbh->query($query, \PDO::FETCH_CLASS, '\stdClass');
		}
		return $result;
	}
	
	public function sth($query = null, $bindings = array())
	{
		$this->log[]= $query;
		$sth = null;
		if ($this->dbh) {
			$sth = $this->dbh->prepare($query);
			$exec = @$sth->execute($bindings);
			if (false === $exec) {
				$sth = $this->dbh->prepare($query);
				$sth->execute($bindings);
			}
		}
		return $sth;
	}
	
	public function insert($query = null, $bindings = array())
	{
		//$this->bug['db'][]= array(array($query, __FILE__, __LINE__), 'Log');
		$this->dbh->beginTransaction();
		$sth = $this->sth($query, $bindings);
		$id = $this->dbh->lastInsertId();
		$this->dbh->commit();
		return $id;
	}
	
	public function select($query = null, $bindings = array(), $useReadPdo = true)
    {
		$sth = $this->sth($query, $bindings);
		return $result = $sth->fetchAll(\PDO::FETCH_OBJ);

	}
	
	/* 这个要改 where order limit */
    public function fetchAll($query = null, $bindings = array(), $fetch_style = \PDO::FETCH_OBJ)
    {
        $sth = $this->sth($query, $bindings);
        if ($sth) {
            return $sth->fetchAll($fetch_style);
        }
        return false;
    }
	
	public function fetchObject($query = null, $bindings = array())
    {
		$sth = $this->sth($query, $bindings);
		$result = $sth->fetchObject();
		
		//print_r(array($result, gettype($result)));exit;
		return $result;
	}
	
	/* 拼接 */
        public function sql_add($obj = null, $batch = null)
        {
            if ($batch) {
                $sql = array();
                foreach ($obj as $row) {
                    $sql []= $this->sql_add($row);
                }
                return $str = implode(';' . PHP_EOL, $sql);
            }
            
            if (is_object($obj)) {
                    $obj = (array) $obj;
		}
		
		foreach ($obj as $k => $v) {
			if (is_array($v)) {
				$v = implode(',', $v);
				if (!$v) {
					$v = null;
				}
			}
			//$v = mysql_real_escape_string($v);
			$v = addslashes($v);
			$obj[$k] = $v;
		}
		//print_r($obj);exit;
		$arr = $obj;
		//print_r($obj);
		
		$keys = array_keys($arr);
		$field = implode('`, `', $keys);
		$fld = "`$field`";
		
		$values = array_values($arr);
		$value = implode("', '", $values);
		$val = "'$value'";
		
		/*
		$count = count($values);
		$s = array();
		for ($i=0; $i<$count+1; $i++) {
			$s[$i] = '?';
		}
		$str = implode(", ", $s);
		//echo $str;
		*/
		
		//echo $fld, PHP_EOL;
		//var_dump($values);
		$sql = "INSERT INTO {$this->_name} ($fld) VALUES ($val)";
		
		//echo $sql;exit;
		return $sql;
        }
        
        public function sql_findAll($where = null, $order = null, $limit = '0,100', $select = '*', $count = null)
	{
            if (1 == $count) {
                $select = 'COUNT(1) AS `i`';
                $limit = 1;
                
            } elseif (0 === $count) {
                $limit = 1;
                
            } elseif (is_string($count)) {
                $select = "`$count`";
                //$limit = 1;
            }
            
            $sql = "SELECT $select ";
            $sql .= " FROM {$this->_name} ";
            
            $where = $this->sql_where($where);
            if ($where) {
                    $sql .= " WHERE $where ";
            }
			if ($this->groupBy) {
                    $sql .= " GROUP BY {$this->groupBy} ";
            }
            if ($order) {
                    $sql .= " ORDER BY $order ";
            }
			
            if ($limit) {
                    $sql .= " LIMIT $limit ";
            }
            return $sql;
        }
        
	public function find($id = null, $select = '*')
	{
		if (!is_numeric($id)) {
			$id = 0;
		}
		
		$sql = "SELECT $select FROM {$this->_name} WHERE `id` = '$id' LIMIT 1";
		
		$row = $this->fetchObject($sql);
		return $this->returnResult($row, $select);
	}
	
	public function add($obj = null)
	{
		if (!$obj) {
			return null;
		}
		$sql = $this->sql_add($obj);
		
		$values = null;
		return $this->insert($sql, $values);
	}
        
        public function up($data = array(), array $where = array(), $reserve_name = 'updated', $reserve_value = null)
	{
            if ($reserve_name) {
                if (null === $reserve_value) {
                    $reserve_value = time();
                }
                $data[$reserve_name] = $reserve_value;
            }
		
		
		$SET = array();
		foreach ($data as $k => $v) {
                    if (is_array($v)) {
                        print_r($v);exit;
                    }
			$v = addslashes($v);
			$SET []= "`$k` = '$v'";
		}
		$set = implode(', ', $SET);
		
		
		$sql = "UPDATE `{$this->_name}` SET $set ";
                
                $and = array();
                foreach ($where as $w) {
                    $and []= $w;
                }
                $and = implode(' AND ', $and);
                if ($and) {
                    $sql .= "WHERE $and ";
                }
         
                //$this->bug['db'][]= array(array($sql, __FILE__, __LINE__), 'Log');        
		return $this->update($sql);
	}
        
        public function fetchRow($data = null, $select = '*', $order = null, $limit = 1)
        {
            $sql = "SELECT $select FROM {$this->_name}";

            $and = $this->sql_where($data);//echo exit;
            
            if ($and) {
                $sql .= " WHERE $and";

                if ($order) {
            		if ($order) {
	                    $sql .= " ORDER BY $order";
		            }
                }
            }
            $sql .= " LIMIT $limit";//echo exit;
            
            //$this->internet->debug_(array(array($sql, __FILE__, __LINE__)), 1);

            return $this->fetchObject($sql);
        }
        
        public function sql_where($data)
        {
            $and = array();
            if (is_array($data)) {
                foreach ($data as $k => $v) {
                    $v = addslashes($v);
                    $binary = '';
                    if (preg_match('/^BINARY\s+/i', $k)) {
                        $binary = 'BINARY ';
                        $k = preg_replace('/^BINARY\s+/i', '', $k);
                    }
                    $and []= "$binary`$k` = '$v'";
                }
                $and = implode(' AND ', $and);
            } else {
                $and = $data;
            }
            
            return $and;
        }
        
        public function getRow($data = array(), $select = '*')
        {
            return $this->fetchRow($data, $select);
        }
        
	/*
         * 返回数据
         */
        
	public function returnResult($row = null, $select = '*')
	{
		if (!$row) {
			return $row;
		}
		
		if (preg_match('/(\*|\,)/', $select, $matches)) {
			//print_r(array(__FILE__, __LINE__, $matches, $row));//exit;
			return $row;
			
		} else if (preg_match('/^(.*)\s+AS\s+(.*)$/i', $select, $matches)) {
			//print_r(array(__FILE__, __LINE__, $matches));exit;
			$select = $matches[2];
			
		} else if (!preg_match('/^([0-9a-z_]+)$/i', $select, $matches)) {
			print_r(array(__FILE__, __LINE__, $matches));exit;
		}
		return $row->$select;
	}
        
	public function findAll($where = null, $order = null, $limit = '0,100', $select = '*', $count = null)
	{
            $sql = $this->sql_findAll($where, $order, $limit, $select, $count);
	
            //$this->bug['db'][]= array(array($sql, __FILE__, __LINE__), 'Log');

            $all = $this->fetchAll($sql);
            if ($all) {
                if (1 == $count) {
                    return $all[0]->i;

                } elseif (0 === $count) {
                    return $all[0];
                    
                } elseif (is_string($count)) {
                    //$row = $all[0];
                    //return $row->$count;
					
                    $a = array();
                    foreach ($all as $r) {
                        $a[]= $r->$count;
                    }
                    return $a;
                }
                
            } else {
                if (1 == $count) {
                    return 0;

                } elseif (0 === $count) {
                    return;
                    
                } elseif (is_string($count)) {
                    //return $select;
                }
            }
            return $all;
	}
	
	function __destruct()
	{
            foreach ($this->log as $l) {
                $this->bug['db'][]= array(array($l, __FILE__, __LINE__), 'Log');
            }
	}
}

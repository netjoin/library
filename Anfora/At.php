<?php
namespace Anfora;
class At
{
	public $debug = 0;
	public $bug = array();
	
	public $param = array();

    public function __construct(){
        global $_BUG;
		$this->bug =& $_BUG;
    }

    public function debug_($info = array(), $exit = null)
    {
        if (1 == $this->debug) {
            print_r($info);

            if (1 == $exit) {
                exit;
            }
        }
    }
	
	public function debug($info = array(), $exit = null)
    {
        if ($this->debug) {
            $this->bug []= $info;
            

            if (-1 == $exit) {
                //print_r($this->bug);exit;
                include_once MODULE_PATH .'\Robot\View/debug.php';
                exit($exit);
            }
        }
    }
	
	public function setVar(array $var = array())
    {
        foreach ($var as $k => $v) {
            $this->$k = $v;
        }
    }
	
	public function getParam($key = null, $value = null)
    {
        return $param = isset($this->param[$key]) ? $this->param[$key] : $value;
    }
    
    public function reservedVariable($key = null, $default = false, $filter = FILTER_DEFAULT, $variables = null)
    {
		/*if (null === $variables) {
			$variables = $_GET;
		}*/
        $var = isset($variables[$key]) ? $variables[$key] : null;
        if (null !== $var) {
            if (is_array($var)) {
                return $var;
            }
            if (is_array($filter)) {
                if (in_array($var, $filter)) {
                    $var = false;
                } 
            } else {
                $var = filter_var($var, $filter);
            }
            if (false !== $var) {
                return $var;
            }
        }
        return $default;
    }
	
	public function get($key = null, $default = false, $filter = FILTER_DEFAULT)
    {
        return $this->reservedVariable($key, $default, $filter, $_GET);
    }
    
    public function post($key = null, $default = false, $filter = FILTER_DEFAULT)
    {
        return $this->reservedVariable($key, $default, $filter, $_POST);
    }
	
	public function request($key = null, $default = false, $filter = FILTER_DEFAULT)
    {
        return $this->reservedVariable($key, $default, $filter, $_REQUEST);
    }

    public function __destruct() {
		//global $_BUG;rint_r($_BUG);
    }
}

<?php
namespace Anfora;
class View {

    public $_dir_ = '';
    public $_module_ = '';
    public $_file_ = '';

    function __construct($file = null, array $param = array(), array $arg = array('_dir_' => '', '_module_' => '')) {
        // 设置类的变量
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        $this->_file_ = $file;

        // 设置内部变量
        
        /*
        foreach ($param as $key => $value) {
            $$key = $value;
        }*/
       
        extract($param);
        unset($param);
        
        /* 
        // 清除定义外的变量
        foreach (get_defined_vars() as $k => $v) {
            if ('this' == $k) {
                unset($k, $v);
                break;
            }
            //elseif (!in_array($k, array('file'))) {

              //} 
            eval("unset(\$" . $k . ");");
        }*/

        //print_r(get_defined_vars());exit;

        
        $root_path = defined('ROOT_PATH') ? ROOT_PATH : PROJECT_ROOT;
        $f = $this->_dir_ ? : $root_path . '/module/' . ($this->_module_ ? : MODULE_NAME) . '/View/' . $this->_file_ . '.php';
		$f = str_replace('\\', '/', $f);
		include_once $f;
    }

    /* */

    public function e($str = '') {
        return urlencode($str);
        return htmlspecialchars($str);
        return htmlentities($str);
    }

    public function basename($path = null, $suffix = null) {
        $filename = $path;
        if (is_array($path)) {
            $extension = trim(array_pop($path));
            $filename = $path[0];
            if ($extension) {
                $filename .= ".$extension";
            }
        }
        return $filename;
    }

    public function extension($ext = null, $prefixReplace = 0) {
        $ext = trim($ext);
        $s = ltrim($ext, '.');
        if ($s && !$prefixReplace) {
            $s = '.' . $s;
        }
        return $s;
    }

    public function highlight($str = null, $pattern = null, $prefix = null, $suffix = null) {
        $pattern = preg_replace('/[\^\$]/', ' ', $pattern);
        $pattern = trim($pattern);
        $replacement = $prefix . $pattern . $suffix;
        //$s = preg_replace($pattern, $replacement, $str);
        $s = str_replace($pattern, $replacement, $str);
        return $s;
    }

    public function checkVar($name = null) {
        print_r($GLOBALS);
        return get_defined_vars();
    }

    public function queryString(array $arr = array()) {
        parse_str($_SERVER['QUERY_STRING'], $QUERY);
        $QUERY = array_merge($QUERY, $arr);
        return http_build_query($QUERY);
    }
    
    public function buildQuery($new = array(), $old = array())
    {
        if (!$old) {
            $old = $_SERVER['QUERY_STRING'];
        }
        if (!is_array($old)) {
            parse_str($old, $old);
        }
        
        if ($new) {
            if (!is_array($new)) {
                parse_str($new, $new);
            }
        }
        
        $query_data = array_merge($old, $new);
        return http_build_query($query_data);
    }
	
    public function pageLinks($pagination)
    {
        $queryPages = $this->buildQuery("page={$pagination->pages}");
        $queryPage = $this->buildQuery("page=1");

        $str = "<a href=\"?$queryPage\">{$pagination->page}</a> / <a href=\"?$queryPages\">{$pagination->pages}</a>页 ";
        if ($pagination->previous) {
                $queryPrevious = $this->buildQuery("page={$pagination->previous}");
                $str .= "<a href=\"?$queryPrevious\">上一页</a> ";
        }
        if ($pagination->next) {
                $queryNext = $this->buildQuery("page={$pagination->next}");
                $str .= "<a href=\"?$queryNext\">下一页</a> ";
        }
        return $str;
    }
    
    public function columnNames($col, $order = null, $sort = 'name', $tpl = null)
    {
        if (!$tpl) {
            $tpl = "\t\t\t\t" .'<td><a href="?sort={sort}&order={order}">{name}</a>{arrow}</td>'. PHP_EOL;
        }
        
        $str = '';
        foreach ($col as $k => $v) {
            $ord = $order;
            $arrow = '';
            if ($k == $sort) {
                if ('desc' != $order) {
                    $ord = 'desc';
                    $arrow = '↑';
                } else {
                    $ord = '';
                    $arrow = '↓';
                }
            }
            $row = str_replace('{name}', $v, $tpl);
            $row = str_replace('{arrow}', $arrow, $row);
            $row = str_replace('{order}', $ord, $row);
            $row = str_replace('{sort}', $k, $row);
            $str .= $row;
        }
        return $str;
    }

    public function __destruct() {
        
    }

}

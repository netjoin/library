<?php
namespace Anfora;

/**
 * Description of Controller
 *
 * @author Benny
 */
class Controller extends At
{
    public $action = null;
    
    public $vars = null;
    public $controllerFile = null;
    
    public $methods = array();
    public $actionMethod;
    public $disableAutorun = null;
    
    public $page = null;
    public $q = null;
    
    public function __construct(array $var = array())
    {
        parent::__construct();
        
        $this->setVar($var);
        if (1 < func_num_args()) {
            $this->controllerFile = func_get_arg(1);
        }
		
		if (!defined('CONTROLLER_PATH')) {
			define('CONTROLLER_PATH', dirname($this->controllerFile));
		} else {
			print_r(array($this->controllerFile, __FILE__, __LINE__));exit;
		}
        
        
        if (false !== $this->get('debug')) {
            $this->debug = $this->get('debug');
        }
        
        $this->methods = get_class_methods($this);
        $actionMethod = $this->action . 'Action';
        if (in_array($actionMethod, $this->methods)) {
            $this->actionMethod = $actionMethod;
        }
        if (null === $this->actionMethod) {
            $this->debug(array(print_r($this, true), __FILE__, __LINE__));
        }
    }
    
    
    
    
    
    public function run($method = null)
    {
        if (!$method) {
            $method = $this->actionMethod;
        }
        //echo 'NULL' != gettype($method);
        if ($method) {
            return $this->$method();
        }
    }
	
	public function pagination($count, $item = null, $limit = null, $page = null)
	{
		if (!$limit) {
			$limit = $this->get('limit', 100, FILTER_VALIDATE_INT);
		}
			
		if (!$page) {
			$page = $this->get('page', 1, FILTER_VALIDATE_INT);
		}
		
		$pages = ceil($count/$limit);
		
		$previous = $next = $last = null;
		
		if (1 < $page) {
			$previous = $page - 1;
		}
		if ($page < $pages) {
			$next = $page + 1;
		}
		$offset = $limit * $page - $limit;
		$first = $offset + 1;
		if (is_numeric($item)) {
			$last = $offset + $item;
		}
		
		$arr = array(
			'count' => $count,
			'limit' => $limit,
			'page' => $page,
			'pages' => $pages,
			'previous' => $previous,
			'next' => $next,
			'offset' => $offset,
			'first' => $first,
			'last' => $last,
			'item' => $item,
		);
		
		return (object) $arr;
	}
    
    public function __destruct()
    {
        //$this->debug(array(__METHOD__));
        if (1 != $this->disableAutorun) {
            $this->run();
        }
        parent::__destruct();
    }
}

<?php
namespace library\Ext;

use At\Extension;

class PhpMongoDB extends Extension
{
	public $mongodb_driver_manager = null;
	public $mongodb_driver_bulkwrite = null;
	public $mongodb_driver_writeconcern = null;
	
	public function __construct($arg = array()) {
        echo __LINE__ .' '. __FILE__ . PHP_EOL;
        parent::__construct($arg);

		$this->driverBulkWrite();
		$this->driverManager();
		$this->driverWriteConcern();

		echo __LINE__ .' '. __FILE__ . PHP_EOL;
    }

    # MongoDB\Driver\Manager
	public function driverManager($uri = "mongodb://127.0.0.1:27017/", $uriOptions = [], $driverOptions = [])
	{
		$this->mongodb_driver_manager = new \MongoDB\Driver\Manager($uri, $uriOptions, $driverOptions);
		
	}

	public function executeBulkWrite($namespace = 'db.collection', $bulk = null, $writeConcern = null)
	{
		$bulk = $bulk ? : $this->mongodb_driver_bulkwrite;
		if (!count($bulk)) {
			return false;
		}
        $this->driverBulkWrite();
		if (!$writeConcern) {
			$writeConcern = $this->mongodb_driver_writeconcern;
		}
            
            try {
                return $this->result = $this->mongodb_driver_manager->executeBulkWrite($namespace, $bulk, $writeConcern);
            } catch (\MongoDB\Driver\Exception\BulkWriteException $e) {
                
                return $this->mongodb_driver_writeresult = $e->getWriteResult();
            }
		
	}

	public function executeCommand($db = 'admin', $command, $readPreference = null)
	{
		return $cursor = $this->mongodb_driver_manager->executeCommand($db, $command, $readPreference);
	}
	
	public function executeQuery($namespace, $query, $readPreference = null)
	{
		return $this->cursor = $this->mongodb_driver_manager->executeQuery($namespace, $query, $readPreference);
	}
	/*
	public function getReadConcern()
	{
		return $result = $this->mongodb_driver_manager->getReadConcern();
	}
	
	public function getReadPreference()
	{
		return $result = $this->mongodb_driver_manager->getReadPreference();
	}
	
	public function getServers()
	{
		return $result = $this->mongodb_driver_manager->getServers();
	}
	
	public function getWriteConcern()
	{
		return $result = $this->mongodb_driver_manager->getWriteConcern();
	}
	
	public function selectServer($readPreference)
	{
		return $this->server = $this->mongodb_driver_manager->selectServer($readPreference);
	}

	# MongoDB\Driver\Command
	public function driverCommand($document)
	{
		return $command = new \MongoDB\Driver\Command($document);

	}
	*/

	# MongoDB\Driver\Query
	public function driverQuery($filter, $options)
	{
		return $query = new \MongoDB\Driver\Query($filter, $options);

	}


    # MongoDB\Driver\BulkWrite
	public function driverBulkWrite()
	{
		$this->mongodb_driver_bulkwrite = new \MongoDB\Driver\BulkWrite();
	}

	public function count()
	{
		return count($this->mongodb_driver_bulkwrite);
		return $this->mongodb_driver_bulkwrite->count();
	}

	public function delete($filter, $deleteOptions = null)
	{
		return $this->mongodb_driver_bulkwrite->delete($filter, $deleteOptions);
	}
	
	public function insert($document)
	{
		return $this->mongodb_driver_bulkwrite->insert($document);
	}
	
	public function update ($filter, $newObj, $updateOptions = null)
	{
		return $this->mongodb_driver_bulkwrite->update($filter, $newObj, $updateOptions);
	}

	
	# MongoDB\Driver\WriteConcern
	public function driverWriteConcern($w = null, $wtimeout = 1000, $journal = null)
	{
		if (!$w) {
			$w = \MongoDB\Driver\WriteConcern::MAJORITY;
		}
		return $this->mongodb_driver_writeconcern = new \MongoDB\Driver\WriteConcern($w, $wtimeout, $journal);
	}
	/*
	public function WriteConcern_bsonSerialize()
	{
		return $this->mongodb_driver_writeconcern->bsonSerialize();
	}
	
	public function getJournal()
	{
		return $this->mongodb_driver_writeconcern->getJournal();
	}
	
	public function getW()
	{
		return $this->mongodb_driver_writeconcern->getW();
	}
	
	public function getWtimeout()
	{
		return $this->mongodb_driver_writeconcern->getWtimeout();
	}
	
	# MongoDB\Driver\ReadPreference
	public function driverReadPreference($mode, $tagSets = null, $options = [])
	{
		return $this->mongodb_driver_readpreference = new \MongoDB\Driver\ReadPreference($mode, $tagSets, $options);
	}
	
	public function ReadPreference_bsonSerialize()
	{
		return $this->mongodb_driver_readpreference->bsonSerialize();
	}
	
	public function getMaxStalenessSeconds()
	{
		return $this->mongodb_driver_readpreference->getMaxStalenessSeconds();
	}
	
	public function getMode()
	{
		return $this->mongodb_driver_readpreference->getMode();
	}
	
	public function getTagSets()
	{
		return $this->mongodb_driver_readpreference->getTagSets();
	}
	
	
	# MongoDB\Driver\ReadConcern
	public function driverReadConern($level)
	{
		return $this->mongodb_driver_readconern = new \MongoDB\Driver\ReadConcern($level);
	}
	
	public function ReadConern_bsonSerialize()
	{
		return $this->mongodb_driver_readconern->bsonSerialize();
	}
	
	public function getLevel()
	{
		return $this->mongodb_driver_readconern->getLevel();
	}
	
	
	# MongoDB\Driver\Cursor
	public function driverCursor()
	{
		return $this->cursor;
	}
	
	public function getId()
	{
		return $this->cursor->getId();
	}
	
	public function getServer()
	{
		return $this->cursor->getServer();
	}
	
	public function isDead()
	{
		return $this->cursor->idDead();
	}
	public function setTypeMap($typemap)
	{
		return $this->cursor->setTypeMap($typemap);
	}
	public function toArray()
	{
		return $this->cursor->toArray();
	}
        
        # MongoDB\Driver\CursorId
        public function __toString() {
            return (string) $this->getId();
        }
        
        # MongoDB\Driver\Server
	public function driverServer($uri = "mongodb://127.0.0.1/", $uriOptions = [], $driverOptions = [])
	{
		$this->mongodb_driver_server = new \MongoDB\Driver\Manager($uri, $uriOptions, $driverOptions);
		
	}
	public function Server_executeBulkWrite($namespace = 'db.collection', $bulk = null, $writeConcern = null)
	{
		$bulk = $bulk ? : $this->mongodb_driver_bulkwrite;
		if (!count($bulk)) {
			return false;
		}
		if (!$writeConcern) {
			$writeConcern = $this->mongodb_driver_writeconcern;
		}
		return $result = $this->mongodb_driver_server->executeBulkWrite($namespace, $bulk, $writeConcern);
	}
	
	public function Server_executeCommand($db = 'admin', $command, $readPreference = null)
	{
		return $cursor = $this->mongodb_driver_server->executeCommand($db, $command, $readPreference);
	}
	
	public function Server_executeQuery($namespace, $query, $readPreference = null)
	{
		return $this->cursor = $this->mongodb_driver_server->executeQuery($namespace, $query, $readPreference);
	}
        
        public function getHost()
        {
            return $this->server->getHost();
        }
        
        public function Server_getInfo()
        {
            return $this->server->getInfo();
        }
        
        public function getLatency()
        {
            return $this->server->getLatency();
        }
        
        public function getPort()
        {
            return $this->server->getPort();
        }
	
        public function getTags()
        {
            return $this->server->getTags();
        }
	
        public function getType()
        {
            return $this->server->getType();
        }
        
        public function isArbiter()
        {
            return $this->server->isArbiter();
        }
        
        public function isHidden()
        {
            return $this->server->isHidden();
        }
        
        public function isPassive()
        {
            return $this->server->isPassive();
        }
        
        public function isPrimary()
        {
            return $this->server->isPrimary();
        }
        public function isSecondary()
        {
            return $this->server->isSecondary();
        }
        
        # MongoDB\Driver\WriteConcernError
        public function getCode()
        {
            return $this->mongodb_driver_writeconcernerror->getCode();
        }
        
        public function getInfo()
        {
            return $this->mongodb_driver_writeconcernerror->getInfo();
        }
        
        public function getMessage()
        {
            return $this->mongodb_driver_writeconcernerror->getMessage();
        }
       
       
        # MongoDB\Driver\WriteResult 
        public function drvierWriteResult()
        {
            
        }
        public function getDeletedCount()
        {
            return $this->result->getDeletedCount();
        }
        public function getInsertedCount()
        {
            return $this->result->getInsertedCount();
        }
        public function getMatchedCount()
        {
            return $this->result->getMatchedCount();
        }
        public function getModifiedCount()
        {
            return $this->result->getModifiedCount();
        }
        public function getServer()
        {
            return $this->result->getServer();
        }
        public function getUpsertedCount()
        {
            return $this->result->getUpsertedCount();
        }
        public function getUpsertedIds()
        {
            return $this->result->getUpsertedIds();
        }
        
        
        
        public function getWriteConcernError()
        {
            return $this->mongodb_driver_writeconcernerror = $this->mongodb_driver_writeresult->getWriteConcernError();
        }
        public function getWriteErrors()
        {
            return $this->mongodb_driver_writeresult->getWriteErrors();
        }
        public function isAcknowledged()
        {
            return $this->result->isAcknowledged();
        }
        
        # MongoDB\BSON\fromJSON 
        public function fromJSON($json)
        {
            return $this->mongodb_bson_fromjson = new \MongoDB\BSON\fromJSON($json);
        }
        public function fromPHP($value)
        {
            return $this->mongodb_bson_fromphp = new \MongoDB\BSON\fromPHP($value);
        }
        public function toJSON($bson)
        {
            return $this->mongodb_bson_tojson = new \MongoDB\BSON\toJSON($bson);
        }
        public function toPHP($bson, $typeMap = array())
        {
            return $this->mongodb_bson_tophp = \MongoDB\BSON\toPHP($bson);
        }


        // MongoDB\BSON\Binary
        public function Binary($data, $type)
        {
            return $this->mongodb_bson_binary = new \MongoDB\BSON\Binary($data, \MongoDB\BSON\Binary::TYPE_GENERIC);
        }
        
        public function getData()
        {
            return $this->mongodb_bson_binary->getData();
        }
        public function Binary_getType()
        {
            return $this->mongodb_bson_binary->getType();
        }
        public function Decimal128($value)
        {
            return new \MongoDB\BSON\Decimal128($value);
        }
        public function Javascript($code, $scope = null)
        {
            return new \MongoDB\BSON\Javascript($code, $scope);
        }
        
        public function MaxKey()
        {
            return new \MongoDB\BSON\MaxKey();
        }
        public function MinKey()
        {
            return new \MongoDB\BSON\MinKey();
        }
        */
       
        public function ObjectID($id = null)
        {
            return new \MongoDB\BSON\ObjectId($id);
        }

        /*
        // MongoDB\BSON\Regex
        public function bsonReg($pattern, $flags)
        {
            return $this->mongodb_bson_regex = new \MongoDB\BSON\Regex($pattern, $flags);
        }
        public function getFlags()
        {
            return $this->mongodb_bson_regex->getFlags();
        }
        public function getPattern()
        {
            return $this->mongodb_bson_regex->getPattern();
        }
        public function Regex___toString()
        {
            return (string) $this->mongodb_bson->regex;
        }
        // MongoDB\BSON\Timestamp
        public function timestamp($increment, $timestamp)
        {
            return $this->mongodb_bson_timestamp = new \MongoDB\BSON\Timestamp($increment, $timestamp);
        }
        public function Timestamp___toString()
        {
            return (string) $this->mongodb_bson_timestamp;
        }
        // MongoDB\BSON\UTCDateTime
        public function bsonUTCDateTime($milliseconds)
        {
            return $this->mongodb_bson_utcdatetime = new \MongoDB\BSON\UTCDateTime($milliseconds);
        }
        public function toDateTime()
        {
            return $this->mongodb_bson_utcdatetime->toDateTime();
        }
        public function UTCDateTime___toString()
        {
            return (string) $this->mongodb_bson_utcdatetime;
        }

        // MongoDB\BSON\Type
        // MongoDB\BSON\Persistable
       

        # MongoDB\BSON\Serializable
        public function bsonSerializalbe($array1 = null)
        {
            $_ = array('_id' => $this->mongodb_bson_objectid);
            return array_merge($array1, $_);
        }

        # MongoDB\BSON\Unserializable
        public function bsonUnserializable($json)
        {
            class MyDocument implements \MongoDB\BSON\Unserializable
            {
                private $data = [];
                function bsonUnserialize(array $data)
                {
                    $this->data = $data;
                }
            }  
        
            $bson = $this->fromJSON($json);
            return $this->mongodb_bson_unserialzable = $value = $this->toPHP($this->mongodb_bson_fromjson, ['root' => 'MyDocument']);
        }
*/
        # MongoDB\Driver\Exception
}

<?php
namespace library\Libra;

use library\Libra\Func;

class Robot
{

    public $db;
    public $limit = 5;
	public $download = 0;//覆盖已存在的下载文件
    public $parse_category_key = 0;
	public $_url_list_key = 0;
	public $_url_play_key = 'play';
	public $_class_name = null;
	public $_reverse;//页码时间逆向排序
	public $_overwrite = 0;//覆盖,忽略条目最后编辑时间
	public $_page_count = 0;//最大页设置
	public $_last_time = '';//条目最后编辑时间
	public $_reurl = null;
	public $_offset_time = 0;
	public $_type = 0;
	public $_lastTime = 0;
	public $func_format = null;
	
    public function __construct()
    {
        global $_BUG;
        $this->bug =& $_BUG;
		
        ini_set('max_execution_time', 200);
    }

    /* 添加异常处理的 file_get_contents */

    public function file_get_contents_($file = null)
    {
        
        
        try {
            $contents = @file_get_contents($file);
            if (false === $contents) {
                //var_dump($http_response_header);
				$contents = @file_get_contents($file);
				if (false === $contents) {
					throw new \Exception('Division by zero.');
				}
            }
            $this->bug['file'][] = array(array($file, __FILE__, __LINE__), 'Log');
            return $contents;
        } catch (\Exception $e) {
            $msg = 'Caught exception: '. $e->getMessage(). "\n";
            $this->bug['file'][] = array(array($msg, __FILE__, __LINE__), 'Error');
            if (isset($http_response_header)) {
                return $http_response_header;
            }
			
        }
    }

    public function file_put_contents_($file = null, $data = null)
    {
        $dir = dirname($file);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        
        try {
            $size = file_put_contents($file, $data);
            if (false === $size) {
                throw new \Exception("$file");
            }
			$this->bug['file'][] = array(array($file, __FILE__, __LINE__), 'Log');
            return $size;
        } catch (\Exception $ex) {
            $msg = 'Caught exception: '. $ex->getMessage(). "\n";
			$this->bug['file'][] = array(array($msg, __FILE__, __LINE__), 'Error');
        }
    }
	
	/*
	 * 获取远程文件
	 */
    
    public function fsockopen_($url , $cookie = null)
    {
        ini_set('max_execution_time', 120);
        
        $URL = parse_url($url);
        $hostname = $URL['host'];
        $path_query = $URL['path'];
        if (isset($URL['query'])) {
            $path_query .= '?' . $URL['query'];
        }  

        $port = 443;
        if (!$cookie) {
        //$cookie = 'tencentSig=3882626048; RECOMMEND_TIP=true; user_trace_token=20161026011229-a61b6b8188b0403ebd76fa054d6c412a; LGUID=20161026011229-35dd987f-9ad6-11e6-ba0d-525400f775ce; gr_user_id=aa2982b4-ced7-486f-b1e6-38db047791ef; witkey_login_authToken="UXrwYjZRfi2pKc9DbslqaAsLTjeFmjnjg4gw8rby34ZI1kU68owhQgLc4HojMhSJiTzz3snD+HwqY5yxxUNEMB2UosQ6aQtz6BHpRu5BnnVSxKVmXgk4foexlsjj1hgdomhWuZx0LFqjCqriDTmnMfCEF+J0ggNvi0xw/PgTnsR4rucJXOpldXhUiavxhcCELWDotJ+bmNVwmAvQCptcy5e7czUcjiQC32Lco44BMYXrQ+AIOfEccJKHpj0vJ+ngq/27aqj1hWq8tEPFFjdnxMSfKgAnjbIEAX3F9CIW8BSiMHYmPBt7FDDY0CCVFICHr2dp5gQVGvhfbqg7VzvNsw=="; JSESSIONID=A381887ADA9D74609D1022B4188D6CE5; _putrc=58873CA8333BC91F; login=true; unick=Mr.%E5%90%B4; showExpriedIndex=1; showExpriedCompanyHome=1; showExpriedMyPublish=1; hasDeliver=504; SEARCH_ID=aab9bc3f3eb145e0ad33e3a10759c4fb; index_location_city=%E4%B8%8A%E6%B5%B7; _gat=1; PRE_UTM=; PRE_HOST=; PRE_SITE=; PRE_LAND=https%3A%2F%2Fwww.lagou.com%2F; TG-TRACK-CODE=index_deliver; _ga=GA1.2.637430993.1477415544; Hm_lvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1481807872,1483812995; Hm_lpvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1484082414; LGSID=20170111050648-b338136d-d778-11e6-8bc1-5254005c3644; LGRID=20170111050654-b6c5a676-d778-11e6-a0ec-525400f775ce';
        $cookie = 'user_trace_token=20170205001230-bab4270a-eaf4-11e6-9328-525400f775ce; LGUID=20170205001230-bab429a0-eaf4-11e6-9328-525400f775ce; JSESSIONID=B4D477122DBF7A53995CED93DBC8FDC1; PRE_UTM=; PRE_HOST=; PRE_SITE=; PRE_LAND=https%3A%2F%2Fwww.lagou.com%2Fjobs%2F2309970.html; _putrc=58873CA8333BC91F; login=true; unick=Mr.%E5%90%B4; showExpriedIndex=1; showExpriedCompanyHome=1; showExpriedMyPublish=1; hasDeliver=596; LGSID=20170312025158-cdd75f60-068b-11e7-8d5a-525400f775ce; LGRID=20170312025944-e3bb2cf9-068c-11e7-8d5d-525400f775ce; _gat=1; TG-TRACK-CODE=index_navigation; Hm_lvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1487074986; Hm_lpvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1489259666; _ga=GA1.2.1451915457.1486224735; SEARCH_ID=d3d2fa869ae248e0bae95d832ae6c34e; index_location_city=%E4%B8%8A%E6%B5%B7';
		
		$cookie = 'JSESSIONID=ABAAABAAAIAACBI441795FD23405F6478DA448E59FDF3C4; user_trace_token=20171001142713-ed7c890a-0bd4-4e2b-ae4c-8648b80bc175; X_HTTP_TOKEN=b4ed527fef7d0378b929bd4567b3dbe3; _gat=1; PRE_UTM=; PRE_HOST=; PRE_SITE=https%3A%2F%2Fwww.lagou.com%2Fjobs%2Flist_PHP%3Fpx%3Dnew%26yx%3D25k-50k%26city%3D%25E4%25B8%258A%25E6%25B5%25B7; PRE_LAND=https%3A%2F%2Fpassport.lagou.com%2Flogin%2Flogin.html%3Fts%3D1506839232987%26serviceId%3Dlagou%26service%3Dhttps%25253A%25252F%25252Fwww.lagou.com%25252F%26action%3Dlogin%26signature%3D5B6C3F912558F074C6006B021247E78A; LGUID=20171001142713-9017be18-a671-11e7-9372-5254005c3644; _putrc=58873CA8333BC91F; login=true; unick=Mr.%E5%90%B4; showExpriedIndex=1; showExpriedCompanyHome=1; showExpriedMyPublish=1; hasDeliver=597; index_location_city=%E4%B8%8A%E6%B5%B7; _gid=GA1.2.1278092600.1506839248; _ga=GA1.2.1727458129.1491699362; Hm_lvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1506839248; Hm_lpvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1506839272; LGSID=20171001142713-9017bbba-a671-11e7-9372-5254005c3644; LGRID=20171001142735-9d4aaa04-a671-11e7-9372-5254005c3644; TG-TRACK-CODE=index_navigation';
    }
     $connection = 'keep-alive';
     $connection = 'close';
$string = "GET $path_query HTTP/1.1
Host: $hostname
Connection: $connection
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36
Referer: https://www.lagou.com/jobs/list_PHP?px=new&yx=25k-50k&city=%E4%B8%8A%E6%B5%B7
Cookie: $cookie

";
//echo $string;exit;
        $str = false;
        $s = '';
		
		//echo $hostname, $port, $errno, $errstr;exit;
        $fp = fsockopen('ssl://' . $hostname, $port, $errno, $errstr, 30);
        if (!$fp) {
            echo "$errstr ($errno)";exit;
        } else {
            fwrite($fp, $string);

            while (!feof($fp)) {
                $s .= fgets($fp, 128);
            }
            fclose($fp);
            //echo $s;
            $S = preg_split("/\r\n\r\n/m", $s, 2);
            //print_r($S);exit;
            if (array_key_exists(1, $S)) {
                $s = trim($S[1]);
                //$s = preg_replace("/^1eb9|0$/m", '', $s);
                //$s = preg_replace("/\r\n|1000/m", '', $s);
                
                $str = trim($s);
                
                $STR = preg_split("/\r\n/m", $str);
                //print_r($STR);exit;
                $str = '';
                $i = 0;
                foreach ($STR as $a) {
                    if ($i%2 == 0) {
                        
                    } else {
                        $str .= $a;
                    }
                    $i++;
                }
                //echo $str;exit;
            }
        }
        return $str;
    }
    
    public function file_get_contents_context($url)
    {
        $optionget = array('http' => array('method' => "GET", 'header' => "User-Agent:Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.21022; .NET CLR 3.0.04506; CIBA)\r\nAccept:*/*\r\nReferer:https://kyfw.12306.cn/otn/leftTicket/init"));
        return $file_contents = file_get_contents($url, false, stream_context_create($optionget));
    }
    
    public function curl_get($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 600);
        $result = @curl_exec($ch);
        curl_close($ch);
        return $result;
    }
	
    

    public function file_read($filename, $length = null, $start = -1, $end = 100)
    {
        $str = false;
        if ($filename) {
            if ($handle = fopen($filename, 'rb')) {
                if (!$length) {
                    $length = filesize($filename);
                }
                //$str = fread($handle, $length);
                $str = '';
                $i = 0;
                /*
                while (!feof($handle)) {
                    if ($i > $start) {
                        if ($i < $end) {
                            $str .= fread($handle, $length);
                        }
                    }
                    
                    $i++;
                }*/
				while (($buffer = fgets($handle)) !== false) {
					
					if ($i > $start) {
                        if ($i < $end) {
							//echo $buffer;
                            $str .= $buffer;
							
                        } else {
							break;
						}
                    }
					$i++;
				}
				/*
				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}*/
                fclose($handle);
            }
        }
        return $str;
    }

    
    
    
    public function getUrlContents($key = 0, $_1 = null, $_2 = null, $_3 = null, $_4 = null, $_5 = null, $_6 = null)
    {
        $url = $this->getUrl($key, $_1, $_2, $_3, $_4, $_5, $_6);//echo exit;
        return $this->file_get_contents_($url);
    }
    
    /*
	 * 写入本地文件
	 */
    public function putFile($key = 0, $_1 = null, $_2 = null, $_3 = null, $_4 = null, $_5 = null, $_6 = null)
    {
        $file = $this->getPath($key, $_1, $_2, $_3, $_4, $_5, $_6);//echo exit;
        if ($filesize = $this->can_download($file)) {
            return array($filesize);
        } else {
            $data = $this->getUrlContents($key, $_1, $_2, $_3, $_4, $_5, $_6);
            return $this->file_put_contents_($file, $data);
        }
    }
	
	/*
	 * 读取本地文件
	 */
	public function getPathContents($key = 0, $_1 = null, $_2 = null, $_3 = null, $_4 = null, $_5 = null, $_6 = null)
    {
        $url = $this->getPath($key, $_1, $_2, $_3, $_4, $_5, $_6);//echo exit;
        return $this->file_get_contents_($url);
    }

	/*
	 * 获取属性配置
	 */
    public function getPath($key = 0)
    {
        $tpl = $this->paths[$key];
        for ($i=1; $i<func_num_args(); $i++) {
            $arg = func_get_arg($i);
            $tpl = preg_replace("/%$i/", $arg, $tpl);
        }
        return $tpl;
    }

    public function getUrl($key = 0)
    {
        $tpl = $this->urls[$key];
        for ($i=1; $i<func_num_args(); $i++) {
            $arg = func_get_arg($i);
            $tpl = preg_replace("/%$i/", $arg, $tpl);
        }
        return $tpl;
    }
	
	public function can_download($file, $size = 1)
    {
        if (!$this->download && file_exists($file) && $size < ($filesize = filesize($file))) {
            return $filesize;
        }
        return false;
    }
	
	/*
	 * 下载文件
	 */

    public function downloadList()
    {
        $key = $this->_url_list_key;
        $result = $this->putFile($key, $this->page);
        $data = $this->getPathContents($key, $this->page);
        if (preg_match('/^\<!DOCTYPE/i', $data)) {
            unlink($this->getPath($key, $this->page));
            $result = $this->putFile($key, $this->page);
            //print_r(array($result, __FILE__, __LINE__));exit;
            $data = $this->getPathContents($key, $this->page);
        }
		
		$rss = $this->getSimpleXMLElement($data, __METHOD__, $key);
		/*foreach ($rss->list[0]->children() as $r) {
			$last = (string) $r->last[0];
			$now = strtotime($last);
			print_r(array($now));
		}*/
		$video = null;
		if ($rss) {
			$video = $rss->list[0]->children();
			$pageCount = $this->getPageCount($rss->list[0]);
		} else {
			$video = $this->xmlParse($data, 'video');
			$rss = $this->xmlParse($data, 'list', 1);
			$pageCount = $this->getPageCount($rss);
		}
		//print_r(array($rss, $video, __FILE__, __LINE__));exit;
		$lastTime = $this->getLastTime($video);
        
        return array(
            'result' => $result,
            'pageCount' => $pageCount,
			'lastTime' => $lastTime,
        );
    }

    public function downloadListJson()
    {
        
        $result = $this->putFile($this->_url_list_key, $this->page);
        $json = $this->getPathContents($this->_url_list_key, $this->page);
        $obj = json_decode($json);//print_r(array($obj->data[0]->vod_addtime));exit;
        @$pageCount = $obj->page->pagecount ? : 1;
        
        $lastTime = $this->getLastTimeJson($obj, '');
        
        return array(
            'result' => $result,
            'pageCount' => $pageCount,
            'lastTime' => $lastTime,
        );
    }
	
	public function downloadDetail()
    {
        $key = $this->parse_category_key;
        $result = $this->putFile($key, $this->page);
        //$data = $this->getPathContents($key, $this->page);
        //$lastTime = $this->getLastTime($rss->list[0]->children());
        
        return array(
            'result' => $result,
            'pageCount' => $this->_page_count,
			'lastTime' => $this->_last_time,
        );
    }
	
	public function downloadPlay()
    {
        $urlSiteDetail = new \library\Model\UrlSiteDetail;

        $where = "status = 1 AND site_id = '$this->site_id'";
        if ($this->id) {
            $where .= " AND id = '$this->id'";
            $this->download = 1;
        }

        $count = $urlSiteDetail->findAll($where, "id", "0,10", "id", 1);
        $pageCount = ceil($count / $this->limit);

        $offset = $this->limit * $this->page - $this->limit;
        $all = $urlSiteDetail->findAll($where, "id", "$offset,$this->limit", "detail_id,mcid,name,area");

        $result = array();
        foreach ($all as $row) {//print_r($row);exit;
            $name = $row->detail_id;
            $result []= $this->putFile($this->_url_play_key, $name);
        }

        return array(
            'result' => $result,
            'pageCount' => $pageCount,
        );
    }
	
	/*
	 * 匹配文件内容
	 */
	public function xmlParse($xml, $tag = null, $type = null, $allowable_tags = null)
	{
		$xml = preg_replace("/<!\[CDATA\[/", '', $xml);//{CDATA}
		$xml = preg_replace("/\]\]>/", '', $xml);//{|CDATA}
		$xml = preg_replace("/<dd([\s0-9a-z=\"]*)>/", "{dd$1}", $xml);
		$xml = preg_replace("/<\/dd>/", "{|dd}", $xml);
		if (!$allowable_tags) {
			$allowable_tags = 'img,a,rss,class,list,video,last,id,tid,name,type,pic,lang,area,year,state,note,actor,director,dl,dd,des,CDATA';
		}
		$tags = explode(',', $allowable_tags);
		$tags = implode('><', $tags);
		$tags = "<$tags>";
		$xml = strip_tags($xml, $tags);
		
		$preg_split = preg_split("/<\/$tag>/i", $xml);
		//print_r($preg_split);
		$arr = array();
		foreach ($preg_split as $split) {
			$partten = "/<$tag([\s0-9a-z=\"]*)>(.*)/i";
			if (preg_match($partten, $split, $matches)) {
				//print_r($matches);
				
				$attr = $matches[1];
				if (!$type) {
					$inner = $matches[2];
					$tg = new \stdClass;
					if (preg_match_all("/<([a-z]+)([\s0-9a-z=\"]*)>((?!<\/).)*<\/([a-z]+)>/i", $inner, $matches2)) {
						//print_r($matches2);
						$tagcontent = $matches2[0];
						$ta = $matches2[1];
						
						$i = 0;
						foreach ($ta as $t) {
							$val = strip_tags($tagcontent[$i]);
							if ('dl' == $t) {
								$ar = array();
								if (preg_match_all("/{([a-z]+)([\s0-9a-z=\"]*)}((?!{\|).*){\|([a-z]+)}/i", $val, $matches4)) {
									//print_r($matches4);
									$tag_dd = $matches4[1];
									
									$att = $matches4[2];
									$val_dd = $matches4[3];
									$j = 0;
									$tag_d = '';
									foreach ($tag_dd as $tag_d) {
										$at = $this->matchAttr($att[$j]);
										$ar[] = array($val_dd[$j], $at);
										$j++;
									}
									//$tg->$tag_d = array($ar);
								}
								$std = new \library\Model\StdClass($ar);
								$tg->$t = array($std);
								//exit;
							} else {
								$tg->$t = array($val);
							}
							
							$i++;
						}
					}
					$arr[] = $tg;
				} else {
					$arr = $this->matchAttr($attr, $arr);
				}
			}
		}
		return $arr;
	}
	
	public function matchAttr($attr, $arr = array())
	{
		if (preg_match_all("/([a-z]+)=\"([^\s]+)\"/", $attr, $matches3)) {
			//print_r($matches3);
			$name = $matches3[1];
			$value = $matches3[2];
			$i = 0;
			foreach ($name as $nm) {

				$arr[$nm] = $value[$i];
				$i++;
			}
		}
		return $arr;
	}
	
	public function getCategoryId($str)
	{
		$arr = array('', '电影', '电视剧', '综艺', '动漫', '动作片', '喜剧片', '爱情片', '科幻片', '恐怖片', '剧情片', '战争片', '国产', '港台', '日韩', '欧美', 119 => '海外');
		$arr = array_flip($arr);
		$id = isset($arr[$str]) ? $arr[$str] : -1;
		return $id;
	}
	
	
	
	public function getMatch($str, $key = null, $start = '开始', $end = '结束')
	{
		if (is_array($key)) {
			$arr = array();
			foreach ($key as $k => $value) {
				$arr[$k] = $this->getMatch($str, $value, $start, $end);
			}
			return $arr;
		}
		return $this->getMatchs($str, $start, $end, "<!--影片$key%1代码-->");
	}
	
	public function getMatchs($str, $start = null, $end = null, $tpl = null)
	{
		$value = null;
		if ($tpl) {
			$start = preg_replace("/%1/", $start, $tpl);
			$end = preg_replace("/%1/", $end, $tpl);
		}
		$partten = "/$start(.*)$end/s";
		//echo $partten = "/<!--原始播放数据开始-->(.*)<!--播放列表结束代码-->/s";exit;
		if (preg_match($partten, $str, $matches)) {
			//print_r($matches);
			$value = $matches[1];
		}
		return $value;
		print_r(array($end, $start, $tpl));exit;
	}

	/*
	 * 解析
	 */
    public function parseCategory()
    {
        $cat = new \library\Model\UrlSiteCategory;
        $data = $this->getPathContents($this->parse_category_key, 1);
        
        $rss = $this->getSimpleXMLElement($data, __METHOD__);
		if ($rss) {
			$ty = $rss->class[0]->children();
		} else {
			$ty = $this->xmlParse($data, 'class');
			print_r(array(__FILE__, __LINE__, $ty));exit;
		}
        
        $class = array();
        foreach ($ty as $row) {
            $class[] = array((int) $row['id'], (string) $row);
        }
        
        $result = array();
        foreach ($class as $row) {
            $arr = array('identification' => $row[0], 'name' => $row[1], 'site_id' => $this->site_id);
            $result []= $cat->checkRow($arr);
        }
        
        return array(
            'result' => $result,
            'pageCount' => 1,
        );
    }

    public function parseCategoryJson()
    {
        $cat = new \library\Model\UrlSiteCategory;
        $json = $this->getPathContents($this->_url_category_key, 1);
        $obj = json_decode($json);
        $list = $obj->list;
        $result = array();
        foreach ($list as $row) {
            $row->site_id = $this->site_id;
            $arr = Func::array_change_key($row, array('list_id' => 'identification', 'list_name' => 'name'));
            $result []= $cat->checkRow($arr);
        }
        
        return array(
            'result' => $result,
            'pageCount' => 1,
			'format' => 'json',
        );
    }
	
    public function parseCategoryArray($class)
    {
        $cat = new \library\Model\UrlSiteCategory;
        
        
        $result = array();
        foreach ($class as $key => $value) {
            if (!is_numeric($value)) {
                $arr = array('identification' => $key, 'name' => $value, 'site_id' => $this->site_id);
                $result []= $cat->checkRow($arr);
            }
        }
        
        return array(
            'result' => $result,
            'pageCount' => 1,
        );
    }
	
    public function parseList()
    {
		$msg = $lastTime = '';
        $rss = null;
		$result = array();
		
        $detail = new \library\Model\UrlSiteDetail;
        $detail->site_id = $this->site_id;
        $data = $this->getPathContents($this->_url_list_key, $this->page);
        
		$rss = $this->getSimpleXMLElement($data, __METHOD__);
		
		
		$video = null;
		if ($rss) {
			$video = $rss->list[0]->children();
			$pageCount = $this->getPageCount($rss->list[0]);
		} else {
			$videos = $this->xmlParse($data, 'video');
			$video = $videos;
			//
			$rss = $this->xmlParse($data, 'list', 1);
			$pageCount = $this->getPageCount($rss);
		}
		
		$arr = array();
		$i = 0;
		foreach ($video as $r) {
			//print_r($r);exit;
			$url = '';
			$dl = isset($r->dl) ? $r->dl[0] : null;
			
			if ($dl) {
				//if (is_callable($dl->children())) {
					$dd = $dl->children();
				/*} else {
					$dl = $videos[$i]->dl[0];
					$dd = $dl->children();
					print_r($dd);
					print_r($video);exit;
				}*/
				
				foreach ($dd as $d) {
					//print_r($d);
					$url .= (string) $d . PHP_EOL;
				}
			} //else { print_r($dl);}
			
			$row = array();
			$row['addtime'] = (string) $r->last[0];
			$row['detail_id'] = (int) $r->id[0];
			$row['cid'] = (int) $r->tid[0];
			$row['name'] = (string) $r->name[0];
			$row['pic'] = (string) $r->pic[0];
			$row['language'] = (string) $r->lang[0];
			$row['area'] = (string) $r->area[0];
			$row['year'] = (int) $r->year[0];
			$row['detail_status'] = (int) $r->state[0];
			@$row['note'] = (string) $r->note[0];
			@$row['actor'] = (string) $r->actor[0];
			@$row['director'] = (string) $r->director[0];
			@$row['content'] = (string) $r->des[0];
			if ($this->_type) {
				$row['cid'] = $this->typeCid($r);
			}
			if ($this->_reurl) {
				$row['reurl'] = $this->getUrl($this->_reurl, $row['detail_id'], $row['cid']);
			} elseif (isset($r->reurl[0])) {
				$row['reurl'] = (string) $r->reurl[0];
			}
			$row['url'] = $url;
			$row['site_id'] = $this->site_id;
			$arr []= $row;
			$i++;
		}
		
		foreach ($arr as $row) {
			$result []= $detail->checkRow($row);
			/*
			ob_flush();
			flush();
			sleep(1);
			*/
		}
		
		return array(
            'result' => $result,
            'pageCount' => $pageCount,
			'lastTime' => $this->getLastTime($video),
			'siteId' => $this->site_id,
        );
    }

    public function parseListJson()
    {
        $detail = new \library\Model\UrlSiteDetail;
        $json = $this->getPathContents($this->_url_list_key, $this->page);
        $obj = json_decode($json);
        @$data = $obj->data ? : [];
        @$pageCount = $obj->page->pagecount ? : 1;
        
        $lastTime = $this->getLastTimeJson($obj, '', $detail);
        
        
        $arr2 = array(
            'vod_id' => 'detail_id',
            'vod_cid' => 'cid',
            'vod_mcid' => 'mcid',//
            'vod_name' => 'name',
            'vod_stitle' => 'stitle',//
            'vod_doubanid' => 'douban_id',
            'vod_area' => 'area',
            'vod_year' => 'year',
            'vod_director' => 'director',
            'vod_actor' => 'actor',
            'vod_title' => 'title',
            'vod_continu' => 'continu',
            'vod_total' => 'total',
            'vod_language' => 'language',
            'vod_content' => 'content',
            'vod_pic' => 'pic',
            'vod_diantai' => 'diantai',//
            'vod_letter' => 'letter',//
            'vod_prty' => 'prty',//
            'vod_tvcont' => 'tvcont',//
            'vod_addtime' => 'addtime',
            'vod_filmtime' => 'filmtime',
            'vod_url' => 'url',
            'vod_keywords' => 'keywords',
            'vod_stars' => 'stars',
            'vod_isend' => 'isend',
            'vod_color' => 'color',//
            'vod_play' => 'play',
            'vod_hits' => 'hits',
            'vod_hits_day' => 'hits_day',//
            'vod_hits_month' => 'hits_month',//
            'vod_hits_lasttime' => 'hits_lasttime',//
            'vod_hits_week' => 'hits_week',//
            'vod_down' => 'down',//
            'vod_up' => 'up',//
            'vod_server' => 'server',
            'vod_gold' => 'gold',//
            'vod_golder' => 'golder',//
            'vod_reurl' => 'reurl',
            'vod_jumpurl' => 'jumpurl',//
            'vod_skin' => 'skin',//
            'vod_inputer' => 'inputer',
            'vod_isfilm' => 'isfilm',
            'vod_length' => 'length',
            'vod_weekday' => 'weekday',
            'vod_status' => 'detail_status',
            'list_name' => 0,
            'vod_type' => 0,
			'vod_copyright' => 0,
			'vod_state' => 0,
			'vod_version' => 0,
			'vod_tv' => 0,
			'vod_douban_id' => 'douban_id',
			'vod_series' => 0,
        );
        
        $result = array();
        foreach ($data as $row) {
			/*$keys = array_keys((array) $row);
			sort($keys);
			$key = array_keys((array) $arr2);
			sort($key);*/
            $row->site_id = $this->site_id;
            if ($this->_reurl) {
                $row->vod_reurl = $this->getUrl($this->_reurl, $row->vod_id);
            }
            $arr = Func::array_change_key($row, $arr2);
            # print_r(array($keys, $key, $arr, $row));exit;
            $result []= $detail->checkRow($arr);
        }
        
        return array(
            'result' => $result,
            'pageCount' => $pageCount,
            'lastTime' => $lastTime,
			'siteId' => $this->site_id,
        );
    }
	
	public function parseDetail()
	{
		/*$filename = $this->getPath($this->parse_category_key, $this->page);
		$doc = new \DOMDocument;
        @$doc->loadHTMLFile($filename);*/
		$result = array();
		$data = $this->getPathContents($this->parse_category_key, $this->page);
		if ($data) {
			$arr = array(
				'pic' => '图片', 
				'name' => '名称',
				'actor' => '演员',
				'director' => '导演',
				'stitle' => '备注',
				'cid' => '类型',
				'mcid' => '大类型',
				'area' => '地区',
				'addtime' => '更新时间',
				'continu' => '状态',
				'language' => '语言',
				'content' => '介绍',
			);
			$arr = $this->getMatch($data, $arr);
			$url = $this->getMatchs($data, '<!--原始播放数据开始-->', '<!--播放列表结束代码-->');
			$url = preg_replace("/<!--|-->/", '', trim($url));
			$url = preg_replace("/{br}/", PHP_EOL, $url);
			$arr['url'] = $url;
			$arr['year'] = $this->getMatchs($data, '<!--影片年份:', '-->');
			$arr['detail_id'] = $this->page;
			$arr['site_id'] = $this->site_id;
			$arr['cid'] = $this->getCategoryId($arr['cid']);
			$arr['reurl'] = "http://www.myzyzy.com/?s=vod-read-id-$this->page.html";
			//print_r($arr);exit;
			
			$UrlSiteDetail = new \library\Model\UrlSiteDetail;
			$result = $UrlSiteDetail->checkRow($arr);
			
		} else {
			$this->_last_time = date('Y-m-d H:i:s');
		}
		
		return array(
            'result' => $result,
            'pageCount' => $this->_page_count,
			'lastTime' => $this->_last_time,
        );
	}
	
	public function parsePlay()
    {
        $urlSiteDetail = new \library\Model\UrlSiteDetail;

        $where = "status = 1 AND site_id = '$this->site_id'";
        if ($this->id) {
            $where .= " AND id = '$this->id'";
        }

        $count = $urlSiteDetail->findAll($where, "id", "0,10", "id", 1);
        $pageCount = ceil($count / $this->limit);

        $offset = $this->limit * $this->page - $this->limit;
        $all = $urlSiteDetail->findAll($where, "id", "$offset,$this->limit", "id,detail_id,mcid,name,area,url");

        $result = array();
        foreach ($all as $row) {//print_r($row);exit;
            $this->_url_list_key = $this->_url_play_key;
			$this->page = $row->detail_id;
			
			$update = $this->parseList();
			$result []= $update;
        }

        return array(
            'result' => $result,
            'pageCount' => $pageCount,
        );
    }
	
	/*
	 * 更新
	 */
	public function updateDetail()
    {
        $play = $this->downloadPlay();
        $parse = $this->parsePlay();

        $result = array(
                'download' => $play,
                'parse' => $parse,
            );

        return array(
            'result' => $result,
            'pageCount' => 1,
        );
    }
	
	/*
	 * 读文件和数据库
	 */
	public function setLastTime()
	{
		$addtime = null;
		$key = 'lastTime_' . $this->site_id;
		if (isset($_COOKIE[$key])) {
			if (!$this->_lastTime) {
				$addtime = $_COOKIE[$key];
			}
		} else {
			$detail = new \library\Model\UrlSiteDetail;
			$row = $detail->fetchRow(['site_id' => $this->site_id], 'addtime', 'addtime DESC');
			if ($row && $row->addtime) {
				$addtime = $row->addtime;
				setcookie($key, $addtime, 0, '/');
			}
		}
		return $addtime;
	}
	
	public function getLastTime($video, $lastTime = null, $detail = null)
	{
		//print_r(array($video));exit;
		//return $video[0]->last[0] . '==';
		$reverse = $this->_reverse;
		if (1 == $reverse || $this->_overwrite) {
			return $lastTime;
		}
		
		/*
		if (!$detail) {
			$detail = new \library\Model\UrlSiteDetail;
		}
		$row = $detail->fetchRow(['site_id' => $this->site_id], 'addtime', 'addtime DESC');
		*/
		
		$time = $this->setLastTime();
		//print_r(array($detail, $row, $video));exit;
		
		//if ($row && $row->addtime) {
		if ($time) {	
			foreach ($video as $r) {
				
				$lastTime = (string) $r->last[0];
				if (!$reverse) {
					break;
				}
			}
			//print_r(array($lastTime, $r));exit;
			if ($lastTime) {
				$last = strtotime($time) + $this->_offset_time;
				$now = strtotime($lastTime);
				//print_r(array($lastTime,  $time));
				//exit;
				//print_r(array($last, $now));
				if ($last <= $now) {
					$lastTime = '';
				} else {
					$lastTime = $time . ' > ' . $lastTime;
				}
				//print_r(array($lastTime,  $time));exit;
			}
		}
		return $lastTime;
	}

    public function getLastTimeJson($obj, $lastTime = null, $detail = null)
    {
		//print_r($obj);
        $reverse = $this->_reverse;
        if (1 == $reverse || $this->_overwrite) {
            return $lastTime;
        }
        
		/*
        if (!$detail) {
            $detail = new \library\Model\UrlSiteDetail;
        }
        $row = $detail->fetchRow(['site_id' => $this->site_id], 'addtime', 'addtime DESC');
		*/
		
		$time = $this->setLastTime();
        //echo $time;exit;
        if ($time) {
            $last = strtotime($time) + $this->_offset_time;
            $now = 0;
			$addtime = '';
			if (isset($obj->data[0]->vod_addtime)) {
				$addtime = $obj->data[0]->vod_addtime;
				$now = strtotime($addtime);
			}
            if ($last <= $now) {
            } else {
                $lastTime = $time . ' > ' . $addtime;//print_r($row, true)
            }
        }
        return $lastTime;
    }
	/**/
	public function getSimpleXMLElement($data, $method = null, $key = null, $page = null)
    {
		$key = $key ? : $this->parse_category_key;
		$page = $page ? : $this->page;
		
		$msg = '';
        $rss = null;
		try {
			
			@$rss = new \SimpleXMLElement($data);
			if (!$rss) {
				throw new \Exception("Value must be 1 or below");
			}
		} catch (\Exception $e) {
			$msg = $e->getMessage();
			$rss = false;
		}
		
		if (!$rss) {
			$log = new \library\Model\RobotLog;
			$log->checkRow(array(
					'name' => $this->_class_name, 
					'function' => __FUNCTION__, 
					'method' =>$method, 
					'key' => $key, 
					'page' => $page, 
					'exception' => $msg,
					'type' => 4,
				));
		}
        return $rss;
    }
	
	public function getPageCount($rss, $page = null, $max = 1499)
    {
		$page = $page ? : $this->page;
		
		$pageCount = 1;
		
		if ($rss) {			
			/*$attr = $rss->list[0]->attributes();			
			foreach ($attr as $key => $value) {
				if ('pagecount' == $key) {
					$pageCount = (int) $value;
					break;
				}
			}*/
			
			$pageCount = (int) $rss['pagecount'];
			
		} else {
			if ($max > $page) {
				$pageCount = 1 + $page;
			}
		}
        return $pageCount;
    }

    public function __destruct()
    {
        
    }
}

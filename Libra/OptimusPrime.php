<?php
namespace library\Libra;
class OptimusPrime extends Transformers
{
    public $dir = null;
    
    public function __construct()
    {
        $this->dir = __DIR__;
    }
    public function scandir($path)
    {
        if (!$path) {
            $path = $this->dir;
        }
        return $array = scandir($path);
    }
}


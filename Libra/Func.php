<?php
namespace library\Libra;
class Func
{
    public function __construct() {
        
    }
     
    // 替换数组的键名
    static public function array_change_key($obj, array $arr, $clean = null)
    {
        $data = array();
        foreach ($obj as $key => $value) {
            $index = $key;
            if (array_key_exists($key, $arr)) {
                $k = $arr[$key];
                if (is_numeric($k)) {
                    if (!$k) {
                        $index = null;
                    } elseif (1 == $k) {
                        //$index = $key;
                    }
                } else {
                    $index = $k;
                }
            } elseif ($clean) {
                $index = null;
            }
            
            if ($index) {
                $data[$index] = $value;
            }
        }
        
        return $data;
    }
   
    // 值匹配的数组交集
    static public function array_diff_value($arr, $arr2)
    {
        $diff = array();
        foreach ($arr  as $key => $value) {
            if (isset($arr2[$key])) {
                $value2 = $arr2[$key];
                if (is_array($value)) {
                    if (!in_array($value2, $value)) {
                        $diff[$key]= $value;
                    }
                } elseif ($value != $value2) {
                    $diff[$key]= $value;
                }
                
            } else {
                $diff[$key]= $value;
            }
        }
        return $diff;;
    }
    
    // 合并数组的值
    static public function array_values_merge($arr, $unique = null)
    {
        $data = array();
        foreach ($arr as $value) {
            $data = array_merge($data, $value);
        }
        if ($unique) {
            $data = array_unique($data);
        }
        return $data;
    }
    
    // 删除数组并集的键
    static public function array_filter_intersect($arr, $arr2 = null)
    {
        foreach ($arr as $key => $value) {
            if (in_array($key, $arr2)) {
                unset($arr[$key]);
            }
        }
        return $arr;
    }
}

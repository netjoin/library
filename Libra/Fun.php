<?php
namespace library\Libra;
class Fun
{
	public function __construct()
	{
	}

	// 替换数组的键名
    static public function array_change_key($obj, array $arr, $clean = null)
    {
        $data = array();
        foreach ($obj as $key => $value) {
            $index = $key;
            if (array_key_exists($key, $arr)) {
                $k = $arr[$key];
                if (is_numeric($k)) {
                    if (!$k) {
                        $index = null;
                    } elseif (1 == $k) {
                        //$index = $key;
                    }
                } else {
                    $index = $k;
                }
            } elseif ($clean) {
                $index = null;
            }
            
            if ($index) {
                $data[$index] = $value;
            }
        }
        
        return $data;
    }
}

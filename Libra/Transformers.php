<?php
namespace library\Libra;

//use library\Libra\Robot;
//use Model\DbTable\BusLine;
//use Model\DbTable\BusStation;
//use Model\DbTable\BusStationName;
//use Model\DbTable\BusRoute;
//use Model\DbTable\BusRouteStation;
//use Model\DbTable\BusAutoPosition;
//use Model\DbTable\ViewBusLineStation;

class Transformers extends Robot
{
    public $download = 0;
	
    public $table_layer = array(
            'provincetable' => 1,
            'citytable' => 2,
            'countytable' => 3,
            'towntable' => 4,
            'villagetable' => 5,
    );
    
	
    public function __construct($var = array()) {
        /*$this->cache_dir = CACHE_ROOT . '/http/220.178.249.25';
        parent::__construct($var);
        $this->busLine = new BusLine;
        $this->busStation = new BusStation;
        $this->busStationName = new BusStationName;
        $this->busRoute = new BusRoute;
        $this->busRouteStation = new BusRouteStation;
        $this->busAutoPosition = new BusAutoPosition;
        $this->viewBusLineStation = new ViewBusLineStation;*/
    }
    
	
    public function getPath($key = 0)
    {
        $a = array(
            $this->cache_dir . "/7080/sdhyschedule/PhoneQueryAction!getLineInfo.shtml.js",
            $this->cache_dir . "/7080/sdhyschedule/PhoneQueryAction!getLineStation.shtml/lineCode=%1.js",
            $this->cache_dir . "/7006/IMEI=F8757C76-FD83-4EEF-9901-5F84464B0098/lineCode=%1&sxx=%2.js",
            $this->cache_dir . "/7080/sdhyschedule/PhoneQueryAction!getVersion.shtml/version=%1.js",
            $this->cache_dir . "/7080/sdhyschedule/PhoneQueryAction!getZDXX.shtml.js",
            $this->cache_dir . "/7006/GetNextBusTime/xl=%1&sxx=%2.js",
            $this->cache_dir . "/7006/SumComeTime/xl=%1&sxx=%2&zd=%3.js",
        );
        
        $tpl = $a[$key];
        for ($i=1; $i<func_num_args(); $i++) {
            $arg = func_get_arg($i);
            $tpl = preg_replace("/%$i/", $arg, $tpl);
        }
        return $tpl;
    }
    
	
    public function getUrl($key = 0)
    {
        $a = array(
            "http://220.178.249.25:7080/sdhyschedule/PhoneQueryAction!getLineInfo.shtml",
            "http://220.178.249.25:7080/sdhyschedule/PhoneQueryAction!getLineStation.shtml?lineCode=%1",
            "http://220.178.249.25:7006/?IMEI=F8757C76-FD83-4EEF-9901-5F84464B0098&lineCode=%1&sxx=%2",
            "http://220.178.249.25:7080/sdhyschedule/PhoneQueryAction!getVersion.shtml?version=%1",
            "http://220.178.249.25:7080/sdhyschedule/PhoneQueryAction!getZDXX.shtml",
            "http://220.178.249.25:7006/GetNextBusTime?xl=%1&sxx=%2",
            "http://220.178.249.25:7006/SumComeTime?xl=%1&sxx=%2&zd=%3",
        );
        
        $tpl = $a[$key];
        for ($i=1; $i<func_num_args(); $i++) {
            $arg = func_get_arg($i);
            $tpl = preg_replace("/%$i/", $arg, $tpl);
        }
        return $tpl;
    }
	
    public function getPathContents($key = 0, $_1 = null, $_2 = null, $_3 = null, $_4 = null, $_5 = null, $_6 = null)
    {
        $url = $this->getPath($key, $_1, $_2, $_3, $_4, $_5, $_6);//echo exit;
        return $this->file_get_contents_($url);
    }
    
	
    public function getUrlContents($key = 0, $_1 = null, $_2 = null, $_3 = null, $_4 = null, $_5 = null, $_6 = null)
    {
        $url = $this->getUrl($key, $_1, $_2, $_3, $_4, $_5, $_6);//echo exit;
        return $this->file_get_contents_($url);
    }
    
	
    public function putFile($key = 0, $_1 = null, $_2 = null, $_3 = null, $_4 = null, $_5 = null, $_6 = null)
    {
        $file = $this->getPath($key, $_1, $_2, $_3, $_4, $_5, $_6);//echo exit;
        if ($filesize = $this->can_download($file)) {
            return array($filesize);
        } else {
            $data = $this->getUrlContents($key, $_1, $_2, $_3, $_4, $_5, $_6);
            return $this->file_put_contents_($file, $data);
        }
    }
    
	
	//下载路线列表
    public function downloadLine_info()
    {
        $result = $this->putFile(0);
        return array(
            'result' => $result,
            'pageCount' => 1,
        );
    }
    
	
	//解析路线列表
    public function parseLine_info()
    {
        $json = $this->getPathContents(0);
        $obj = json_decode($json);
        $lineList = $obj->lineList;

        $w = array();
        foreach ($lineList as $r) {
                $w []= array($r->lineCode, $r->lineName);
        }//print_r($w);exit;
        
        $where = $this->getWhere($w, 1);
        $result = $this->busLine->checkAll($where);
        
        return array(
            'result' => $result,
            'pageCount' => 1,
        );
    }
    
    public function getWhere($w, $type = null)
    {
        $where = array();
        switch ($type) {
            case 'station_name':
                foreach ($w as $r) {
                    $where []= array(
                        'name' => $r[0],
                    );
                }
                break;
        
            case 1:
                foreach ($w as $r) {
                    $where []= array(
                        'code' => $r[0],
                        'name' => $r[1],
                    );
                }
                break;
            
            case 2:
                foreach ($w as $r) {
                    $where []= array(
                        'name' => $r[0],
                        'longitude' => $r[1],
                        'latitude' => $r[2],
                    );
                }
                break;

            case 3:
                foreach ($w as $r) {
                    $where []= array(
                        'code' => $r[0],
                        'direction' => $r[1],
                        'stations' => $r[2],
                        'begin_station' => $r[3],
                        'end_station' => $r[4],
                        'begin_time' => $r[5],
                        'end_time' => $r[6],
                    );
                }
                break;

            case 4:
                foreach ($w as $r) {
                    $where []= array(
                        'name' => $r[0],
                        'longitude' => $r[1],
                        'latitude' => $r[2],
                        'code' => $r[3],
                        'sequence' => $r[4],
                        'direction' => $r[5],
                    );
                }
                break;

            case 5:
                foreach ($w as $r) {
                    $where []= array(
                        'bus_code' => $r[0],
                        'line_code' => $r[1],
                        'longitude' => $r[2],
                        'latitude' => $r[3],
                        'pass_station' => $r[4],
                        'direction' => $r[5],
                    );
                }
                break;

            default:
                break;
        }
        return $where;
    }
    
	//下载路线站点列表
    public function downloadLine_station()
    {
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "status=1";
        $all = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id,code');
        $count = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        
        $result = array();
        foreach ($all as $row) {
            $result [$row->id]= $this->putFile(1, $row->code);
        }
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
    
	//解析路线站点列表
    public function parseLine_station()
    {
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "status=1";
        $all = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id,code');
        $count = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        $result = array();
        foreach ($all as $row) {
            $result [$row->id]= $this->getRoute($row);
        }
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
	
	public function getRoute($row)
	{
		$json = $this->getPathContents(1, $row->code);
        $obj = json_decode($json);
		$lineList = $obj->lineList;
		$busTimeList = $obj->busTimeList;
		$stationList = $obj->stationList;
		
		/**/
		$w = $sxx = array();
		foreach ($lineList as $r) {
			$w [$r->sxx]= array($r->lineCode, $r->sxx, $r->maxOrder, $r->beginStation, $r->endStation);
		}
		
		foreach ($busTimeList as $r) {
			$beginTime = $r->beginTime;
			$endTime = $r->endTime;
			$w [$r->sxx][]= substr($beginTime, 0, 2) .':'. substr($beginTime, 2);
			$w [$r->sxx][]= substr($endTime, 0, 2) .':'. substr($endTime, 2);
			$sxx []= $r->sxx;
		}
		
		for ($i=0; $i<2; $i++) {
			if (!in_array($i, $sxx)) {
				$w [$i][]= '';
				$w [$i][]= '';
			}
		}
		//print_r($w);exit;
        
        $where = $this->getWhere($w, 3);
        $route = $this->busRoute->checkAll($where);
		
		
		$odd = $even = $data = array();
		foreach ($stationList as $r) {
			$data []= array($r->stationName, $r->lon, $r->lat, $r->lineCode, $r->stationOrder, $r->sxx);
			/*if ($r->sxx) {
				$even []= $data;
			} else {
				$odd []= $data;
			}*/
		}//print_r($data);exit;
		$where = $this->getWhere($data, 4);
        $routeStation = $this->busRouteStation->checkAll($where);
		return array($route, $routeStation);
	}
    
    //下载汽车定位信息列表
    public function downloadAuto_position()
    {
        $this->download = 1;
        
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "status=1";
        $all = $this->busRoute->findAll($where, 'id', "$offset, $limit", 'id,code,direction');
        $count = $this->busRoute->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        $result = array();
        foreach ($all as $row) {
            $result [$row->id]= $this->putFile(2, $row->code, $row->direction);
        }
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
    
	//解析汽车定位信息列表
    public function parseAuto_position()
    {
        $this->download = 1;
        
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "status=1";
        $all = $this->busRoute->findAll($where, 'id', "$offset, $limit", 'id,code,direction');
        $count = $this->busRoute->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        $result = array();
        foreach ($all as $row) {
            $result [$row->id]= $this->getPosition($row);
            //print_r($all);exit;
        }
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
	
    public function getPosition($row)
    {
        $file = $this->getPath(2, $row->code, $row->direction);
        if (1 > filesize($file)) {
            return array('404 Not Found', $file);
        }

        $json = $this->getPathContents(2, $row->code, $row->direction);
        $arr = json_decode($json);//print_r($arr);exit;
		
        $data = array();
        foreach ($arr as $r) {
            $data []= array($r->busCode, $r->lineCode, $r->lon, $r->lat, $r->passStation, $r->sxx);
        }//print_r($data);exit;

        $where = $this->getWhere($data, 5);
        $find = array('logitude', 'latitude', 'pass_station');
        return $this->busAutoPosition->checkAll($where, 1, null);
    }
    
	//下载站点信息列表
    public function downloadStation_info()
    {
        $result = $this->putFile(4);
        return array(
            'result' => $result,
            'pageCount' => 1,
        );
    }
    
	//解析站点信息列表
    public function parseStation_info()
    {
        $json = $this->getPathContents(4);
        $obj = json_decode($json);
        $list = $obj->list;

        $w = array();
        foreach ($list as $r) {
            $w []= array($r->ZDNAME, $r->JD, $r->WD, $r->XL, $r->ZD, $r->SXX);
        }//print_r($w);exit;
        
        $where = $this->getWhere($w, 2);
        $result = $this->busStation->checkAll($where);
        
        return array(
            'result' => $result,
            'pageCount' => 1,
        );
    }
    
    # 优化站点名称
    public function optimizeStation_name()
    {
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "";
        $all = $this->busStation->findAll($where, 'id', "$offset, $limit", 'id,name');
        $count = $this->busStation->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        
        
        $array = array();
        foreach ($all as $value) {
            $array []= array($value->name);
        }
        //print_r($array);
        
        $where = $this->getWhere($array, 'station_name');
        $result = $this->busStationName->checkAll($where);
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
    
    # 优化站点线路
    public function optimizeStation_line()
    {
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "";
        $all = $this->busStationName->findAll($where, 'id', "$offset, $limit", 'id,name');
        $count = $this->busStationName->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        
        
        $array = array();
        foreach ($all as $value) {
            $array []= array($value->name);
        }
        //print_r($array);
        
        $where = $this->getWhere($array, 'station_name');
        $result = $this->viewBusLineStation->getLinesByStationNames($where);
        $result = $this->busStationName->updateLines($result, 1);
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
    
    # 优化线路
    public function optimizeLine()
    {
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "status=1";
        $all = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id,code');
        $count = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        $result = array();
        foreach ($all as $row) {
            $stations = $this->busRouteStation->getStationsByLine($row->code);
            $result [$row->id]= $this->busLine->updateStations($stations, $row->id);
        }
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
	
    
	//下载镇列表
    public function downloadDeparture_time()
    {
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "status=1 AND level=4";
        $all = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id,code');//print_r($all);exit;
        $count = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        $year = $this->param[1] ? : 2015;
        $result = array();
        foreach ($all as $row) {
            $result [$row->id]= $this->putFile(4, $year, substr($row->code, 0, 2), substr($row->code, 2, 2), substr($row->code, 4, 2), $row->code);
        }
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
    
	//解析镇列表
    public function parseTown()
    {
        $limit = $this->limit;
        $offset = $this->offset;
        $where = "status=1 AND level=4";
        $all = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id,code');
        $count = $this->busLine->findAll($where, 'id', "$offset, $limit", 'id', 1);
        
        $year = $this->param[1] ? : 2015;
        $result = array();
        foreach ($all as $row) {
            $result [$row->id]= $this->getVillages($row, $year);
        }
        
        return array(
            'result' => $result,
            'pageCount' => ceil($count / $limit),
        );
    }
	
    public function __destruct() {
        parent::__destruct();
    }
}

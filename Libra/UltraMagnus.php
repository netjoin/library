<?php
namespace library\Libra;
class UltraMagnus extends Transformers
{
    public $filename = null;
    public $doc = null;
    public function __construct($var = array()) {
        parent::__construct($var);
    }
    public function loadHTMLFile($filename)
    {
        $this->filename = $filename;
        $this->doc = $doc = new \DOMDocument;
        @$doc->loadHTMLFile($filename);
    }
    public function favicon()
    {
        //$doc = $this->doc;
        $DOMNodelist = $this->doc->getElementsByTagName('link');
        for ($i = 0; $i < $DOMNodelist->length; $i++) {
            $DOMNode = $DOMNodelist->item($i);
            $DOMNamedNodeMap = $DOMNode->attributes;
            $DOMAttr = $DOMNamedNodeMap->getNamedItem('rel');
            if ($DOMAttr) {
                if (preg_match('/(shortcut\s+icon|icon)/i', $DOMAttr->value, $matches)) {
                    $DOMAttr = $DOMNamedNodeMap->getNamedItem('href');
                    return $DOMAttr->value;
                    break;
                }
            }
        }
        return false;
    }
    public function title()
    {
        if (!$this->doc) {
            
        }
        $DOMNodelist = $this->doc->getElementsByTagName('title');
        
            $DOMNode = $DOMNodelist->item(0);
            if ($DOMNode) {
            return $DOMNode->nodeValue;
        }
        return false;
    }
    public function description()
    {
        $meta = get_meta_tags($this->filename);
        if (isset($meta['description'])) {
            return $meta['description'];
        }
        return false;
    }
}


<?php
class Anfora
{
	public function __construct($var = array(), $module = 'Robot', $controller = 'Index', $action = 'index')
    {
        foreach ($var as $k => $v) {
            $this->$k = $v;
        }
        
        $uri = $_SERVER['REQUEST_URI'];
        if (isset($_SERVER['REDIRECT_URL'])) {
            $uri = $_SERVER['REDIRECT_URL'];
        }
        if ($path = strstr($uri, '?', true)) {
            $uri = $path;
        }
	
        if (preg_match("/^\/([^\/]*)(\/|)([^\/]*)(\/|)([^\/]*)(\/|)(.*)/", $uri, $matches)) {
            //print_r($matches);
            $module = $matches[1] ? : 'index'; 
            $controller = $matches[3] ? : $controller;
            $action = $matches[5] ? : $action;
            $param = $matches[7] ? : '';

            $params = explode('/', $param);
            if (is_numeric($controller[0])) {
                $controller = "_$controller";
            }

            $mod = '';
            $split = preg_split('/-/', strtolower($module));
            foreach ($split as $r) {
                 $mod .= ucfirst($r);
            }
            $module = $mod;
            
			$controller = strtolower($controller);
			if ('baidumap' == $controller) {
				$controller = 'baidu-map';
			}
            $controller = ucfirst($controller);
			$c = explode('-', $controller);
			$controller = '';
			foreach ($c as $co) {
				$controller .= ucfirst($co);
			}
                        
            //加载控制器
            $class_name = "module\\$module\Controller\\{$controller}Controller";
            //define('ROOT_PATH', $this->DIR);
            //define('MODULE_PATH', ROOT_PATH . '/module');
            define('MODULE_NAME', $module);
            
            //加载默认控制器
            if (!class_exists($class_name)) {
                $class_name = "module\\$module\Controller\Controller";
            }
            
            //加载默认模块
            if (!class_exists($class_name)) {
                $class_name = "module\Module\Controller\Controller";
            }
            
            $arg = array(
                'action' => $action,
                'param' => $params,
                'vars' => (object) get_defined_vars(),
            );
            
            if (class_exists($class_name)) {
                $controller = new $class_name($arg);
            } else {
                print_r(array(__FILE__, __LINE__, $arg));exit;
            }
        }
    }

	public function __destruct()
	{
		
	}
}
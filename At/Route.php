<?php

namespace At;

class Route
{
    public function __construct($var = array(), $module = 'Robot', $controller = 'Index', $action = 'index')
    {
        foreach ($var as $k => $v) {
            $this->$k = $v;
        }
        
        $uri = $_SERVER['REQUEST_URI'];
        if (isset($_SERVER['REDIRECT_URL'])) {
            $uri = $_SERVER['REDIRECT_URL'];
        }
        if ($path = strstr($uri, '?', true)) {
            $uri = $path;
        }
		
		$uri = urldecode($uri);
		$uri_fixed = str_replace("。", '.', $uri);
		$uri_fixed = preg_replace("/[\|]+/", "/", $uri_fixed);
		$uri_fixed = preg_replace("/[\:]+/", "/", $uri_fixed);
		$uri_fixed = preg_replace("/[\/]+/", "/", $uri_fixed);
	
        if (preg_match("/^\/([^\/]*)(\/|)([^\/]*)(\/|)([^\/]*)(\/|)(.*)/", $uri_fixed, $matches)) {
            //print_r($matches);
            $module = $matches[1] ? : 'index'; 
            $controller = $matches[3] ? : $controller;
            $action = $matches[5] ? : $action;
            $param = $matches[7] ? : '';
			$params = explode('/', $param);

			
			/*
            $mod = '';
            $split = preg_split('/-/', strtolower($module));
            foreach ($split as $r) {
                 $mod .= ucfirst($r);
            }
            $module = $mod;*/
            
			$module = $this->fixClassName($module);
			$controller = $this->fixClassName($controller);
			$action = $this->fixActionName($action);
			/*if ('baidumap' == $controller) {
				$controller = 'baidu-map';
			}*/
			
            /*
			$c = explode('-', $controller);
			$controller = '';
			foreach ($c as $co) {
				$controller .= ucfirst($co);
			}*/
                        
            //加载控制器
            $class_name = "module\\$module\Controller\\{$controller}Controller";
            //define('ROOT_PATH', $this->DIR);
            //define('MODULE_PATH', ROOT_PATH . '/module');
            define('MODULE_NAME', $module);
            
            //加载默认控制器
            if (!class_exists($class_name)) {
                $class_name = "module\\$module\Controller\Controller";
            }
            
            //加载默认模块
            if (!class_exists($class_name)) {
                $class_name = "module\Module\Controller\Controller";
            }
            
            $arg = array(
                'action' => $action,
                'param' => $params,
                'vars' => (object) get_defined_vars(),
            );
            
            if (class_exists($class_name)) {
                $controller = new $class_name($arg);
            } else {
                print_r(array(__FILE__, __LINE__, $arg));exit;
            }
        }
    }
	
	public function fixName($str)
	{
		
		//$str = trim($str, '|');
		
		//$str = preg_replace("/^([\w\-]+)(:|\|)(.*)/i", "$1", $str);
		$str = preg_replace("/\.(php|html|htm)$/i", '', $str);
		$Word = preg_replace("/(\W+)/", ' ', $str);
		if (is_numeric($Word[0])) {
			$Word = "_$Word";
		}
		$word = strtolower($Word);
		$Words = ucwords($word);
		return $_Words = preg_replace("/(\s+)/", '_', $Words);
	}
	
	public function fixClassName($str)
	{
		$name = $this->fixName($str);
		$dot = preg_replace("/(^_|_$)/", '.', $name);
		$Camel = preg_replace("/_/", '', $dot);
		return $_Camel = preg_replace("/\./", '_', $Camel);
	}
	
	public function fixActionName($str)
	{
		$name = $this->fixClassName($str);
		return lcfirst($name);
	}
    
    public function __destruct()
    {
        
    }
}

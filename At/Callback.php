<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace At;

/**
 * Description of Callback
 *
 * @author Benny
 */
class Callback {
    /**
     * array callback
     */
    public static function array_map_callback($file = null) {
        $args = func_get_args();
        if (!$file) {
            if (isset($args[0])) {
                $file = $args[0];
            } else {
                print_r(array(__FILE__, __LINE__, $file, $args));
                exit;
            }
        }

        $file = trim($file, '/');
        $file = str_replace('Controller.php', '', $file);
        return $file;
    }

    public static function array_map_callback_use($file = null, $func = '') {
        if (preg_match('/^' . $func . '(.*)/', $file, $matches)) {
            return $matches[1];
        }
        return $file;
    }

    public static function array_map_callback_methods($file = null) {
        if (preg_match('/(.*)Action$/', $file, $matches)) {
            return $matches[1];
        }
        return $file;
    }

    public static function array_filter_callback($file = null) {
        if (preg_match('/(.*)Action$/', $file, $matches)) {
            return $matches[1];
        }
        return false;
    }

    public static function array_filter_callback_use($file, $func = '') {
        $p = '/^' . $func . '(.*)/';
        if (preg_match($p, $file, $matches)) {
            return $matches[1];
        }
        return false;
    }
    
    function callback2_($file, $func = '') {
        $FUNC = explode('Action', $func);

        $p = '/^' . $FUNC[0] . 'Action(.*)/';
        if (preg_match($p, $file, $matches)) {
            //print_r($matches);exit;
            return $matches[1];
        }
        return false;
    }
}

<?php
namespace At;
class Db
{
	public $dbh = null;
	public $host = 'localhost';
	public $port = 3307;
	public $db_name = 'com_urlnk';
	public $table_name = 'domain_zone';
	public $primary_key = null;
	
	public $db_table = null;
	public $fetch_style = \PDO::FETCH_OBJ;
	
	public $group_by = null;
	public $log = array();
	
	public function __construct($vars = array())
	{
		$this->db_table = $this->db_name .'.'. $this->table_name;
		$this->dbh = $this->getDbh();
	}
	
	/*
	 * 初始化连接
	 */
	public function getDbh()
	{
		$dbh = null;
		$config = array(
			'dns' => "mysql:host=$this->host;port=$this->port;dbname=$this->db_name",
			'username' => "root",
			'password' => "root",
		);
		
		extract($config);
		
		try {
			$dbh = new \PDO($dns, $username, $password);
		} catch (\PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
		
		return $dbh;
	}

	/*
	 * 设置属性
	 */
	public function from($name)
	{
		$this->db_table($name);
		return $this;
	}
	
	public function db_table($name = null)
	{
		return $this->db_table = $name ? : $this->db_name .'.'. $this->table_name;
	}
	
	public function fetch_style($fetch_style = \PDO::FETCH_BOTH)
	{
		if ($fetch_style) {
			$this->fetch_style = $fetch_style;
		}
		return $this;
	}
	
	/*
	 * PDO 操作数据库
	 */
	 
	//增
	public function insert($query, $input_parameters = null, $name = null)
	{
		//echo $query;exit;
		$sth = $this->dbh->prepare($query);
		$sth->execute($input_parameters);
		return $this->dbh->lastInsertId($name);
	}
	
	//查
	public function fetch($query, $input_parameters = null, $fetch_style = null)
	{
		if (!$fetch_style) {
			$fetch_style = $this->fetch_style;
		}
		$sth = $this->dbh->prepare(trim($query));
		$sth->execute($input_parameters);
		return $sth->fetch($fetch_style);
	}
	
	/* 这个要改 where order limit */
    public function fetchAll($query = null, $bindings = array(), $fetch_style = null)
    {
		if (!$fetch_style) {
			$fetch_style = $this->fetch_style;
		}
		
        $sth = $this->sth($query, $bindings);
        if ($sth) {
            return $sth->fetchAll($fetch_style);
        }
        return false;
    }
	
	//获取行
	public function fetchRow($data = null, $select = '*', $order = null, $limit = 1)
	{
		$sql = "SELECT $select FROM $this->db_table";

		$and = $this->sql_where($data);
		
		if ($and) {
			$sql .= " WHERE $and";

			if ($order) {
				if ($order) {
					$sql .= " ORDER BY $order";
				}
			}
		}
		$sql .= " LIMIT $limit";
		return $this->fetchObject($sql);
	}
	
	//改
	public function update($data = array(), array $where = array(), $reserve_name = 'updated', $reserve_value = null)
	{
            if ($reserve_name) {
                if (null === $reserve_value) {
                    $reserve_value = time();
                }
                $data[$reserve_name] = $reserve_value;
            }
		
		
		$SET = array();
		foreach ($data as $k => $v) {
                    if (is_array($v)) {
                        print_r($v);exit;
                    }
			$v = addslashes($v);
			$SET []= "`$k` = '$v'";
		}
		$set = implode(', ', $SET);
		
		
		$sql = "UPDATE " . $this->db_table() . " SET $set ";
                
                $and = array();
                foreach ($where as $key => $w) {
					if (is_numeric($key)) {
						$and []= $w;
					} else {
						$v = addslashes($w);
						$and []= "`$key` = '$v'";
					}
                }
                $and = implode(' AND ', $and);
                if ($and) {
                    $sql .= "WHERE $and ";
                }
         
        //echo $sql;
		
		$this->log[]= $sql;
		//$result = @$this->dbh->query($sql, \PDO::FETCH_OBJ, '\stdClass');
		$result = $this->dbh->exec($sql);
		
		return $result;
	}
	
	//获取对象
	public function fetchObject($query = null, $bindings = array())
    {
		$sth = $this->sth($query, $bindings);
		return $result = $sth->fetchObject();
	}
	
	//执行语句
	public function sth($query = null, $bindings = array())
	{
		$query = trim($query);
		$this->log[]= $query;
		$sth = null;
		if ($this->dbh) {
			$sth = $this->dbh->prepare($query);
			//set_time_limit(400);
			$exec = $sth->execute($bindings);
			/*
			if (false === $exec) {
				$sth = $this->dbh->prepare($query);
				$sth->execute($bindings);
			}
			*/
		}
		return $sth;
	}
	
	//查询
	public function query($query)
	{
        $query = trim($query);
		$this->log[]= $query;
		$result = $this->dbh->query($query, \PDO::FETCH_OBJ, '\stdClass');
		if (false === $result) {
			$result = $this->dbh->query($query, \PDO::FETCH_OBJ, '\stdClass');
		}
		return $result;
	}
	
	/*
	 * 原型 操作数据库
	 */
	 
	//增加一行
	public function addRow($kv)
	{
		$query = $this->getQuery($kv);
		return $this->insert($query);
	}
	
	/*
	 * 添加数据
	 *
	 * @param batch 1 使用第一组的字段 2 一次行执行多条语句 3 执行多次 str 自定义字段
	 */
	public function add($data, $batch = null)
	{
		if (!$batch) {
			$kv = $this->getString($data);
		} elseif (is_numeric($batch)) {
			switch ($batch) {
		        case 1:
				    $kv = $this->getStringByFirst($data);
				    break;
				case 2:
				    $query = $this->getQueryByGroup($data);
				    break;
				default:
				    die(__LINE__ .' '. __FILE__);
			}
		} elseif (is_string($batch)) {
			$kv [0] = $batch;
			$kv [1] = $this->getStringForValues($data);
		} elseif (is_array($batch)) {
			$field = implode("`, `", $batch);
			$field = "`$field`";
			$kv [0] = $field;
			$kv [1] = $this->getStringForValues($data);
		}
		
		//print_r($kv);exit;
		if (3 === $batch) {
			$result = array();
			foreach ($data as $value) {
				$result []= $this->add($value);
			}
			return $result;
			
		} else if (2 === $batch) {
			return $this->insert($query);
		}
		
		return $this->addRow($kv);
	}
	
	public function addSimple($obj = null)
	{
		if (!$obj) {
			return null;
		}
		$sql = $this->sql_add($obj);
		return $this->insert($sql);
	}
	
	
	//获取一行
	public function find($id, $field = '*', $debug = null)
	{
		if (is_array($id)) {
			$where = $this->getWhere($id);
			
		} else {
			if (is_numeric($id)) {
				$where = "$this->primary_key = '$id'";
			} elseif (preg_match("/^([\w_.]+)(\s*)([=|<>!]+|LIKE|NOT)(.*)/", $id, $matches)) {//print_r($matches);exit;
				$where = $id;
			} elseif (preg_match("/^(\s*)([=|<>!]+|LIKE|NOT)/", $id, $matches)) {print_r($matches);exit;
				$where = "$this->primary_key $id";
			} else {
				$where = "$this->primary_key = '$id'";
			}
		}
		
		$query = "SELECT $field FROM $this->db_table WHERE $where LIMIT 1";
		
		$arr = $this->fetch($query);if ($debug) { print_r($this->db_name); }
		if ($arr && preg_match("/^(\w+)$/i", $field, $matches)) {//echo $field;
			return $arr->$field;
		}
		
		return $arr;
	}

	//计算数量
	public function count($where)
	{
		return $count = $this->findAll($where, null, null, null, 1);
	}
	
	//获取多行
	public function findAll($where = null, $order = null, $limit = '0,100', $select = '*', $count = null)
	{
		$sql = $this->sql_findAll($where, $order, $limit, $select, $count);
		
		$all = $this->fetchAll($sql);
		if ($all) {
			if (1 == $count) {
				return $all[0]->i;

			} elseif (0 === $count) {
				return $all[0];
				
			} elseif (is_string($count)) {
				$a = array();
				foreach ($all as $r) {
					$a[]= $r->$count;
				}
				return $a;
			}
			
		} else {
			if (1 == $count) {
				return 0;

			} elseif (0 === $count) {
				return null;
				
			} elseif (is_string($count)) {
				return null;
			}
		}
		return $all;
	 }
	
	/*
	 * SQL 语句
	 */
	
	//插入
	public function getQuery($kv)
	{
		$fields = $kv[0];
		$values = '(' . $kv[1] . ')';
		return $query = "INSERT INTO $this->db_table ($fields) VALUES $values";
	}
	
	//插入多次
	public function getQueryByGroup($data)
	{
		$arr = array();
		foreach ($data as $value) {
		   $arr []= $this->getQuery($value);
		}
		return $query = implode('; ', $arr);
	}
	
	//生成 WHERE 条件 
	public function getWhere($data)
	{
		
		$arr = array();
		foreach ($data as $key => $value) {//print_r($value);exit;
			$value = addslashes($value);
			$arr []= "`$key` = '$value'";
		}
		
		return $str = implode(" AND ", $arr);
	}
	
	public function sql_where($data)
	{
		$and = array();
		if (is_array($data)) {
			foreach ($data as $k => $v) {
				if (!is_numeric($k)) {
					$v = addslashes($v);
					$binary = '';
					if (preg_match('/^BINARY\s+/i', $k)) {
						$binary = 'BINARY ';
						$k = preg_replace('/^BINARY\s+/i', '', $k);
					}
					$and []= "$binary`$k` = '$v'";
				} else {
					$and []= $v;
				}
			}
			$and = implode(' AND ', $and);
		} else {
			$and = $data;
		}
		
		return $and;
	}
	
	public function sql_add($obj = null, $batch = null)
	{
		if ($batch) {
			$sql = array();
			foreach ($obj as $row) {
				$sql []= $this->sql_add($row);
			}
			return $str = implode(';' . PHP_EOL, $sql);
		}
            
        if (is_object($obj)) {
           $obj = (array) $obj;
		}
		
		foreach ($obj as $k => $v) {
			$v = addslashes($v);
			$obj[$k] = $v;
		}
		
		$keys = array_keys($obj);
		$field = implode('`, `', $keys);
		$fld = "`$field`";
		
		$values = array_values($obj);
		$arr = [];
		foreach ($values as $row) {
			/*$quote = $this->dbh->quote($row);
			if ("''" == $quote) {
				$quote = 'null';
			}
			$arr[] = $quote;*/
			$arr[] = addslashes(stripslashes($row));
		}
		$value = implode("', '", $arr);
		# print_r($arr);exit;
		$val = "'$value'";
		
		return $sql = "INSERT INTO $this->db_table ($fld) VALUES ($val)";
    }
	
	
	//获取多行
	public function sql_findAll($where = null, $order = null, $limit = '0,100', $select = '*', $count = null)
	{
		if (1 == $count) {
			$select = 'COUNT(1) AS `i`';
			$limit = 1;
			
		} elseif (0 === $count) {
			$limit = 1;
			
		} elseif (is_string($count)) {
		}
		
		$sql = "SELECT $select ";
		$sql .= " FROM $this->db_table ";
		
		$where = $this->sql_where($where);
		if ($where) {
			$sql .= " WHERE $where ";
		}
		if ($this->group_by) {
			$sql .= " GROUP BY $this->group_by ";
		}
		if ($order) {
			$sql .= " ORDER BY $order ";
		}
		
		if ($limit) {
			$sql .= " LIMIT $limit ";
		}
		return $sql;
	}
	
	/*
	 * SQL 插入键值
	 */
	
	//一对键值
	public function getString($data, $type = null)
	{
		$fieldsValues = $this->getKeysValues($data);//print_r($fieldsValues);exit;
		$fields = $fieldsValues[0];
		$values = $fieldsValues[1];
		$field = implode("`, `", $fields);
		$value = implode("', '", $values);
		$field = "`$field`";
		$value = "'$value'";
		return array($field, $value);
	}
	
	//一键多值
	public function getStringByFirst($data)
	{
		
		$first = $data[0];
		$kv = $this->getString($first);
		$values = array($kv[1]);
		$count = count($data) + 1;
		for ($i = 0; $i < $count; $i ++ ) {
			$values []= $data[$i];
		}
		return $result = array($kv[0], $values);
	}
	
	//值
	public function getStringForValues($data)
	{
	    $kv = $this->getString(array(array(), $data));
		return $kv[1];
	}
	
	/*
	 * 数组功能
	 */
	public function getKeysValues($data)
	{
		$fields = $values = array();
		foreach ($data as $key => $value) {//print_r($value);exit;
			$fields []= $key;
			$values []= addslashes($value);
		}
		return array($fields, $values);
	}
	
	//分页
	public static function getPaginate($count, $current = 1, $limit = 10, $show_pages = 5)
	{
		$total_pages = ceil($count / $limit);
		
		$before = $current - 1;
		$next = $current + 1;
		if (1 > $before) {
			$before = 1;
		}
		if ($next > $total_pages) {
			$next = $total_pages;
		}
		//9 / 5 = 1  1 * 5 = 5 5 + 5 = 10
		$first_no = $limit * $current - $limit + 1;
		$pages = array();
		if ($count > $show_pages) {
			$blocks = ceil($count / $show_pages);
			$in_block = ceil($current / $show_pages);
			$min = 1;
			if (1 != $in_block) {
				$min = $in_block * $show_pages - $show_pages + 1;
			}
			
			$max = $min + $show_pages;
			if ($max > $total_pages) {
				$max = $total_pages + 1;
			}
			
			
			for ($i = $min; $i < $max; $i ++) {
				$pages []= $i;
			}
		}
		
		return (object) $arr = array(
			'items' => array(),
			'current' => $current,
			'before' => $before,
			'next' => $next,
			'last' => $total_pages,
			'total_items' => $count,
			'total_pages' => $total_pages,
			'first_no' => $first_no,
			'show_pages' => $show_pages,
			'pages' => $pages,
			//'debug' => array($blocks, $in_block, $min, $max),
		);
	}
}

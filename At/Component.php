<?php
namespace At;

class Component extends At
{
    //public $param = array();
    public $componentName = 'test';
    public $componentFile = null;
    
    public $componentMethods = array();
    public $componentMethod;
    public $componentAutorun = null;
	
    public $form_method = 'get';
	public $variables = array();

    public function __construct(array $var = array())
    {
        parent::__construct();
        
        $this->setVar($var);
		
		if (!$this->variables) {
			$form_method = '_' . strtoupper($this->form_method);
			eval("\$const = $$form_method;");
			$this->variables = $const;//print_r($this->variables);
		}
		
        if (1 < func_num_args()) {
            $this->componentFile = func_get_arg(1);
        }
		
		$this->componentMethods = get_class_methods($this);
        $actionMethod = $this->componentName . 'Component';
        if (in_array($actionMethod, $this->componentMethods)) {
            $this->componentMethod = $actionMethod;
        }
        if (null === $this->componentMethod) {
           print_r(array(__FILE__, __LINE__, $this));//exit;
        }
    }
    
    public function testComponent()
    {
        echo __FILE__;
    }
    
    public function setVar(array $var = array())
    {
        foreach ($var as $k => $v) {
            $this->$k = $v;
        }
    }

    /*
    public function getParam($key = null, $value = null)
    {
        return $param = isset($this->param[$key]) ? $this->param[$key] : $value;
    }
    
    public function get($key = null, $default = false, $filter = FILTER_DEFAULT)
    {
        $var = isset($_GET[$key]) ? $_GET[$key] : null;
        if (null !== $var) {
            if (is_array($var)) {
                return $var;
            }
            if (is_array($filter)) {
                if (in_array($var, $filter)) {
                    $var = false;
                } 
            } else {
                $var = filter_var($var, $filter);
            }
            if (false !== $var) {
                return $var;
            }
        }
        return $default;
    }
    
    public function post($key = null, $default = false, $filter = FILTER_DEFAULT)
    {
        $var = isset($_POST[$key]) ? $_POST[$key] : null;
        if (null !== $var) {
            if (is_array($var)) {
                    return $var;
            }
            $var = filter_var($var, $filter);
            if (false !== $var) {
                return $var;
            }
        }
        return $default;
    }
    */
    public function run($method = null)
    {
        if (!$method) {
            $method = $this->componentMethod;
        }
        //echo 'NULL' != gettype($method);
        if ($method) {
            return $this->$method();
        }
    }
	
    public function __destruct()
    {
        if ($this->componentAutorun) {
            $this->run();
        }
        parent::__destruct();
    }
}

<?php
namespace At;
class Parser
{
	
	
	public $time = null;
	public $result = null;
	
    public function __construct($url)
	{
		
		$db = new \At\Db();
		
		$this->time = time();
		
		$host = $zone = $zone_id = $name_id = $host_id = $name = $type = $data = $subname = $port = $query = null;
		if (preg_match("/^(microsoft-edge):(.*)/i", $url, $matches)) {//print_r($matches);exit;
			
			$scheme_data = array(
					'name' => $matches[1],
				);
			$scheme_id = $db->from('url_protocol')
							->find($scheme_data, 'id', 0);
			if (!$scheme_id) {
				$scheme_data['created'] = $this->time;
				$scheme_data['status'] = 1;
				$scheme_id = $db->add($scheme_data);
			}
			
			$url = $matches[2];
		}
		
		$URL = parse_url($url);
		if (isset($URL['scheme'])) {
			$scheme = trim($URL['scheme']);
			$scheme_data = array(
					'name' => $scheme,
				);
			$scheme_id = $db->from('url_protocol')
							->find($scheme_data, 'id', 0);
			if (!$scheme_id) {
				$scheme_data['created'] = $this->time;
				$scheme_data['status'] = 1;
				$scheme_id = $db->add($scheme_data);
			}
			
		}
		if (isset($URL['host'])) {
			$host = trim($URL['host']);
			
			if (preg_match("/^xfplay$/i", $scheme, $matches)) {
				$xfplay = $this->xfplay_uri_scheme($URL['host']);
				
				
				//print_r($xfplay);exit;
			
				$data = array(
					'scheme' => $scheme_id,
					'host' => $xfplay->dna,
					'port' => $xfplay->dx,
					'path' => implode('|', $xfplay->zx),
					'filename' => $xfplay->filename,
					'extension' => $xfplay->extension,
					'type' => $this->domain_url_type($xfplay->extension),
				);
				//print_r($data);exit;
				
			} elseif (preg_match("/^jjhd$/i", $scheme, $matches)) {
				$xfplay = $this->jjhd_uri_scheme($URL['host']);
				//print_r($xfplay);exit;
			
				$data = array(
					'scheme' => $scheme_id,
					'host' => $xfplay->hash,
					'port' => $xfplay->number,
					'filename' => $xfplay->filename,
					'extension' => $xfplay->extension,
					'type' => $this->domain_url_type($xfplay->extension),
				);
				//print_r($data);exit;
				
			} elseif (preg_match("/(.*)\.(\w+)$/i", $host, $matches)) {//print_r($matches);//exit;
				$zone = $matches[2];
				$name = substr(strrchr($matches[1], "."), 1) ? : $matches[1];
				$subname = substr($matches[1], 0, strrpos($matches[1], "."));
			}
			
			if ($zone) {
				$zone_id = $db->from('domain_zone')->find("name = '$zone'", "id");
				if (!$zone_id) {
					$zone_data = array(
							'name' => $zone,
							'status' => 1,
							'created' => $this->time,
						);
					$zone_id = $db->add($zone_data);
				}
				
				
				$name_data = array(
						'name' => $name,
						'postfix' => $zone_id,
					);
				$name_id= $db->from('domain_name')
								->find($name_data, 'id', 0);//print_r($name_id);
				if (!$name_id) {
					$name_data['created'] = $this->time;//print_r($name_data);exit;
					$name_data['status'] = 1;
					$name_id = $db->add($name_data);//print_r($host_id);exit;
				}
				
				$port = isset($URL['port']) ? $URL['port'] : null;
				
				
				$host_data = array(
						'domain' => $name_id,
						'subname' => $subname,
						'port' => $port,
					);
					
				//print_r(array($URL, $host_data));exit;
				$host_id = $db->from('domain_host')
								->find($host_data, 'id', 0);
				if (!$host_id) {
					$host_data['created'] = $this->time;
					$host_data['status'] = 1;//print_r($host_data);
					$host_id = $db->add($host_data);
				}
			}
		}
		
		if (isset($URL['path'])) {
			
			if (preg_match("/^ms-word$/i", $scheme, $matches)) {
				$office = $this->office_uri_scheme($URL['path'], $scheme);
			
				$data = array(
					'scheme' => $scheme_id,
					'host' => $office->command_argument[0],
					'port' => $office->command_name,
					'path' => $office->command_argument[1],
					'filename' => $office->filename,
					'extension' => $office->extension,
					'type' => $office->type,
				);
				//print_r($data);exit;
				
			} else {
				$pathinfo = pathinfo($URL['path']);
				
				$query = isset($URL['query']) ? $URL['query'] : null;
				
				$data = array(
					'scheme' => $scheme_id,
					'host' => $host_id,
					'path' => str_replace('\\', '/', $pathinfo['dirname']),
					'filename' => $pathinfo['filename'],
					'extension' => $pathinfo['extension'], 
					'type' => $this->domain_url_type($pathinfo['extension']),
					'query' => $query,
				);
					
			}
			
			
			
		}
		
		if ($data) {
			//$data = array_merge($URL, $data);
			$url_id = $db->from('domain_url')->find($data, 'id', 0);
			if (!$url_id) {
				$data['created'] = $this->time;
				$data['status'] = 1;
				$url_id = $db->add($data);
			}
		}
			
		$this->result = array($zone_id, $name_id, $host_id, $scheme_id, $url_id);
	}
	
	public function office_uri_scheme($path, $scheme = 'ms-word')
	{
		$scheme = strtolower($scheme);
		$preg_split = preg_split("/\|([us])\|/i", $path, null, PREG_SPLIT_DELIM_CAPTURE);//print_r($preg_split);exit;
		$count = count($preg_split);
		
		$component = new \Aquarius\Php\Pathinfo(array('output_type' => 'json', 'variables' => array('path' => $preg_split[2])));
		$result = $component->result();
		
		$command_argument = array('', '');
		$j = 0;
		for ($i = 1; $i < $count; $i ++) {
			if ($i & 1) {
				
				$command_argument[$j] = $preg_split[$i];
				
			} else {
				$command_argument[$j] .= '|'. $preg_split[$i];
				$j ++;
			}
		}
		
		$type = 0;
		switch ($scheme) {
			case 'ms-word':
				$type = 1319.23;
				break;
			default:
				$type = 0;
		}
		
		return (object) $arr = array(
			'command_name' => $preg_split[0],
			'command_argument' => $command_argument,
			'filename' => $result['filename'],
			'extension' => $result['extension'],
			'type' => $type,
		);
		
		
	}
	
	public function xfplay_uri_scheme($host)
	{
		$explode = explode('|', $host);
		
		$info = array();
		foreach ($explode as $exp) {
			$arr = $this->array_string($exp, '=');
			if (!isset($info[$arr->key])) {
				$info[$arr->key] = $arr->value;
			} elseif (is_array($info[$arr->key])) {
				$info[$arr->key] []= $arr->value;
			} else {
				$info[$arr->key] = array($info[$arr->key], $arr->value);
			}
		}
		
		$component = new \Aquarius\Php\Pathinfo(array('output_type' => 'object', 'variables' => array('path' => $info['mz'])));
		$result = $component->result();//print_r($result);exit;
		$info = array_merge($result, $info);
		return (object) $info;
	}
	
	public function jjhd_uri_scheme($host)
	{
		$explode = explode('|', $host);
		
		$arr = array('number', 'hash', 'basename');
		$info = array();
		foreach ($explode as $key => $value) {
			$info[$arr[$key]] = $value;
		}
		
		$component = new \Aquarius\Php\Pathinfo(array('output_type' => 'object', 'variables' => array('path' => $info['basename'])));
		$result = $component->result();//print_r($result);exit;
		$info = array_merge($result, $info);
		return (object) $info;
	}
	
	public function array_string($str, $separator = '|', $key = 0, $value = 1)
	{
		$limit = $flags = null;
		//$flags = PREG_SPLIT_DELIM_CAPTURE;
		
		$preg_split = preg_split("/$separator/i", $str, $limit, $flags);
		//print_r($preg_split);exit;
		
		return (object) array('key' => $preg_split[$key], 'value' => $preg_split[$value]);
	}
	
	public function domain_url_type($extension)
	{
		$type = false;
		if (preg_match("/mp4/i", $extension, $matches)) {
			$type = 22;
		} elseif (in_array(strtolower($extension), array('htm', 'html', 'php', 'asp', 'jsp', 'aspx'))) {
			$type = -8;
		}
		return $type;
	}
	
	public function result()
	{
		return $this->result;
	}
	
	
	
	
	
	public function __destruct()
	{
		
	}
}